#include "include\CTMOS_Config.h"
#include "include\A8008_C_Def.h"
//#include "include\CTMOS_DATA.h"
#include "include\CTI2C_C_FuncTable_Def.h"


#if(CTMOS_I2C_REPL_C_IF==1)
//;===========================================================================
//; INCLUDE
//;===========================================================================



//;===========================================================================
//; DEFINE
//;===========================================================================
//==[STRUCT]==
typedef struct
{
	unsigned char XyHCoord;//XyHCoord[7]=Valid : XyHCoord[6:4]=X_High : XyHCoord[3]=Reserved : XyHCoord[2:0]=Y_High
	unsigned char XLCoord;
	unsigned char YLCoord;
	unsigned char Reserved0;
}CTI2C_C_COORD_XY;

typedef struct
{
	unsigned char Fingers;
	unsigned char Keys;
	CTI2C_C_COORD_XY XyCoord0;
	CTI2C_C_COORD_XY XyCoord1;
	CTI2C_C_COORD_XY XyCoord2;
	CTI2C_C_COORD_XY XyCoord3;
	CTI2C_C_COORD_XY XyCoord4;
}CTI2C_C_ST_COORD;


//;===========================================================================
//; SEGMENT
//;===========================================================================



//;===========================================================================
//; EXTERN
//;===========================================================================
extern	void CTI2C_C_IntSignalLow(void);
extern	void CTI2C_C_IntSignalHigh(void);


extern unsigned char edata	getCTI2C_RegMapMem[];
extern short data gtCTMOS_I2cResX;
extern short data gtCTMOS_I2cResY;
extern unsigned char data gtCTI2C_CmdAddr;
extern unsigned char data gtCTI2C_ByteCnt;
//extern unsigned char edata geCTMOS_CoordTempBuf[];//StCoord

extern unsigned char edata geCTTMX_IdleCaliCnt;//Counting For Calibration, Unit:Frame
//Example: Idle Frame Period = 100ms(10Hz), geCTTMX_IdleCaliCnt = 50
//  Calibration Period = Idle Frame Period * geCTTMX_IdleCaliCnt
// 	Calibration Period = 100 * 50 = 5000ms

extern unsigned char edata geCTMOS_TimerxCntTrimValue;//2msUnit
extern unsigned char edata geCTMOS_TimerxCntSwkIdleValue;//Idle Cnt(Up Count)
//Example 1:IdlePeriodCnt:15Hz->66ms/2ms=33,10Hz->100ms/2ms=50,5Hz->200ms/2ms=100
//Example 2:
//	temp = ((0x100-geCTMOS_TimerxCntTrimValue)*(IdlePeriodCnt))/64);
//	geCTMOS_TimerxCntSwkIdleValue = 0x100 - temp; 
//PS1 : temp Can't Over Flow;
//PS2 : geCTMOS_TimerxCntSwkIdleValue is Zero than Idle To slowest;

extern bit gbtCTSCAN_SetScanActiveFlag;//0->Do Nothing, 1->Idle to Active Scan Mode
extern bit gbCTAP_TouchPalmFlag;//0->Do Nothing, 1->With Touch Palm
//;===========================================================================
//; PUBLIC
//;===========================================================================



//;===========================================================================
//; Variable
//;===========================================================================
CTI2C_C_ST_COORD edata	__geCTI2C_OutCoord;
//unsigned char edata CTSECU_Test[4];



//;===========================================================================
//; Code
//;===========================================================================


//**********************************************************************************************************
//* Function: CTI2C_C_MemCopy                                                                                    
//* Description:                                                                                            
//*      memory copy                                                     
//*                                                                                                             
//* Parameters:                                                                                             
//*		void *dest (input)                                                                                               
//*      	pointer to destination buffer   
//*		void *src (input)                                                                                              
//*      	pointer to source buffer
//*		unsigned int len (input)
//*			buffer size in bytes                                                                                 
//* Returns:                                                                                                
//*      None                                                                                                                                                                                             
//**********************************************************************************************************
void CTI2C_C_MemCopy(void near *dest, void near *src, unsigned char len)
{
	unsigned char near *pDest = dest;
	unsigned char near *pSrc = src;
	if(len == 0) return;
	do
	{
		*pDest++ = *pSrc++;
	 	len--;
	}while(len != 0);
}


//;******************************************************************************
//;* Function: CTI2C_FuncExtendIni
//;* Desc.: 
//;* Param: 
//;*			input :None. 
//;* Return:
//;*			output :None. 
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	Main Loop
//;* Cycles:
//;*	
//;******************************************************************************
void CTI2C_C_FuncExtendIni(void)
{
//==Sample Code, password write in others place bast==
//	CTSECU_Test[0]=0x01;
//	CTSECU_Test[1]=0x02;
//	CTSECU_Test[2]=0x03;
//	CTSECU_Test[3]=0x04;

////==[Enable EXIO]==
	SFR_P1DIR |= 0x02;//P1.1//1->Input
	SFR_P1PU &= ~0x02;//P1.1//0->Disable PU
	SFR_EGTG1 &= ~0x02;//P1.1 Falling Edge
	SFR_EGFG1 &= 0;//Clr Flag
	SFR_EGEN1 |= 0x02;//P1.1
////==[Enable NMI]==
	SFR_NMIF &= ~0x10;
	SFR_NMIE |= 0x10;

//==[Example]==================================================
//==[Set I2C Resolution]==

	getCTI2C_RegMapMem[REG_XY_RESOLUTION_H] = (gtCTMOS_I2cResX>>4 & 0x00F0) | (gtCTMOS_I2cResY>>8 & 0x000F);
	getCTI2C_RegMapMem[REG_X_RESOLUTION_L] = gtCTMOS_I2cResX;
	getCTI2C_RegMapMem[REG_Y_RESOLUTION_L] = gtCTMOS_I2cResY;

	getCTI2C_RegMapMem[0x3F] = 4;//MaxTouch
	getCTI2C_RegMapMem[0xF7] = 3;//Key
}



//;******************************************************************************
//;* Function: CTI2C_IntSignalCtrlHostRdCoordEvent
//;* Desc.: 
//;* Param: 
//;*			input :None. 
//;* Return:
//;*			output :None. 
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	Initial, I2C ISR, ISP, PowerDown, Timer ISR
//;* Cycles:
//;*	
//;******************************************************************************
void CTI2C_C_IntSignalCtrlHostRdCoordEvent(void)
{
	CTI2C_C_IntSignalHigh();	
}

//;******************************************************************************
//;* Function: CTI2C_IntSignalCtrlTrigHostRdCoordEvent
//;* Desc.: 
//;* Param: 
//;*			input :None. 
//;* Return:
//;*			output :None. 
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	Main Loop
//;* Cycles:
//;*	
//;******************************************************************************
void CTI2C_C_IntSignalCtrlTrigHostRdCoordEvent(void)
{
	
	CTI2C_C_IntSignalLow();
}

//;******************************************************************************
//;* Function: CTI2C_HostRdCmdParser
//;* Desc.: 
//;* Param: 
//;*			input :gtCTI2C_CmdAddr. 
//;* Return:
//;*			output :C, 0-> Read Coord Command.
//;*					   1-> Not Read Coord Command. 
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	I2C ISR
//;* Cycles:
//;*	
//;******************************************************************************
bit CTI2C_C_HostRdCmdParser(void)
{
////==[Example]==================================================
	if((gtCTI2C_CmdAddr==0x10)||(gtCTI2C_CmdAddr==0x11)||(gtCTI2C_CmdAddr==0x12))
	{
		CTI2C_C_MemCopy(&getCTI2C_RegMapMem[0x10],&__geCTI2C_OutCoord,sizeof(__geCTI2C_OutCoord));	
		return	0;
	}
	else
	{
		return	1;
	}
}



//;******************************************************************************
//;* Function: CTI2C_WrCmdRangeCk
//;* Desc.: Check wr Cmd range 
//;* Param: 
//;*			input : gtCTI2C_CmdAddr.
//;*			input : gtCTI2C_ByteCnt.
//;*			input : Param0, Host Write Command Parameter
//;* Return:
//;*			output : C, 0-> Store Param0 To I2c Register Map.
//;*						1-> Dump Param0.
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	I2C ISR
//;* Cycles:
//;*	
//;******************************************************************************
bit CTI2C_C_WrCmdRangeCk(unsigned char Param0)
{
	if((gtCTI2C_CmdAddr+gtCTI2C_ByteCnt)==0x02)
	{
		getCTI2C_RegMapMem[0x02]=Param0;
		return	1;
	}
	else
	{
		return	0;
	}	
}



//;******************************************************************************
//;* Function: CTI2C_OSGetDeviceCtrl_Reset
//;* Desc.: 
//;* Param: 
//;*			input : None.
//;* Return:
//;*			output : C, 1-> Reset
//;* Used Register:
//;*		 	
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	Main Loop
//;* Cycles:
//;*	
//;******************************************************************************
bit CTI2C_C_OsGetDeviceCtrlReset(void)
{
	if((getCTI2C_RegMapMem[2] & BIT0) == BIT0)
	{
		return	1;
	}
	else
	{
		return	0;
	}
}



//;******************************************************************************
//;* Function: CTI2C_OSGetDeviceCtrl_PwrDn
//;* Desc.: 
//;* Param: 
//;*			input : None.
//;* Return:
//;*			output : C, 1-> PwrDn
//;* Used Register:
//;*		 	
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	Main Loop
//;* Cycles:
//;*	
//;******************************************************************************
bit CTI2C_C_OsGetDeviceCtrlPwrDn(void)
{
	if((getCTI2C_RegMapMem[2] & BIT1) == BIT1)
	{
		if(( SFR_P1 & 0x02)==0)
		{
			getCTI2C_RegMapMem[2] &= ~BIT1;
			return	0;
		}
		return	1;
	}
	else
	{
		return	0;
	}
}





//;******************************************************************************
//;* Function: CTI2C_FuncExtendMain
//;* Desc.: 
//;* Param: 
//;*			input : None.
//;* Return:
//;*			output : None.
//;* Used Register:
//;*		 	
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	Main Loop
//;* Cycles:
//;*	
//;******************************************************************************
void CTI2C_C_FuncExtendMain(void)
{

////==[Example]===============
////Ex:15Hz->66ms/2ms=33,10Hz->100ms/2ms=50,5Hz->200ms/2ms=100,2Hz->500ms/2ms=250
//	unsigned short temp1;
//	temp1 =	(((0x100-geCTMOS_TimerxCntTrimValue)*50)/64);//Ex:10Hz
//	if(temp1>256)
//		temp1=256;
//
//	geCTMOS_TimerxCntSwkIdleValue = (unsigned char)(0x100 - temp1);
//
////==[Example]===============
//// 	Calibration Period = 100ms * 160 = 16000ms
//	geCTTMX_IdleCaliCnt = 40;//160;
//
////==[Example]===============
//	gbtCTSCAN_SetScanActiveFlag=1;

}


//;******************************************************************************
//;* Function: CTI2C_WrCmdNoParam
//;* Desc.:  
//;* Param: 
//;*			input : gtCTI2C_CmdAddr.
//;*			input : gtCTI2C_ByteCnt.
//;* Return:
//;*			output : None
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	I2C ISR
//;* Cycles:
//;*	
//;******************************************************************************
void CTI2C_C_WrCmdNoParam(void)
{
}


//;******************************************************************************
//;* Function: CTI2C_RdStopIsr
//;* Desc.:  
//;* Param: 
//;*			input : gtCTI2C_CmdAddr.
//;*			input : gtCTI2C_ByteCnt.
//;* Return:
//;*			output : None
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	I2C ISR
//;* Cycles:
//;*	
//;******************************************************************************
void CTI2C_C_RdStopIsr(void)
{
}


//;******************************************************************************
//;* Function: CTI2C_WrStopIsr
//;* Desc.:  
//;* Param: 
//;*			input : gtCTI2C_CmdAddr.
//;*			input : gtCTI2C_ByteCnt.
//;* Return:
//;*			output : None
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	I2C ISR
//;* Cycles:
//;*	
//;******************************************************************************
void CTI2C_C_WrStopIsr(void)
{
}


//;******************************************************************************
//;* Function: CTPWR_2msTimerxIsr
//;* Desc.:  
//;* Param: 
//;*			input : None.
//;* Return:
//;*			output : None.
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	TimerX ISR
//;* Cycles:
//;*	
//;******************************************************************************
void CTPWR_C_2msTimerxIsr(void)
{
}

//;******************************************************************************
//;* Function: CTI2C_C_BufEmpIsr
//;* Desc.:  
//;* Param: 
//;*			input : gtCTI2C_CmdAddr.
//;*			input : gtCTI2C_ByteCnt.
//;* Return:
//;*			output : None
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	I2C ISR
//;* Cycles:
//;*	
//;******************************************************************************
void CTI2C_C_BufEmpIsr(void)
{
}

////;******************************************************************************
////;* Function: CTAP_Coord2OutCoord
////;* Desc.:  
////;* Param: 
////;*			input : geCTMOS_CoordTempBuf[].
////;* Return:
////;*			output : None.
////;* Used Register:
////;*		 	  
////;* Stack:
////;*	Used =  bytes.
////;*	Temp =  bytes.
////;* Notes:
////;*	Main Loop
////;* Cycles:
////;*	
////;******************************************************************************
//void CTI2C_C_StCoord2OutCoord(void)
//{
//	SFR_IE	&= ~BIT7;//Disable All INT
//
//	//St Coord Transfer To Replace Coord
//	CTI2C_C_MemCopy(&__geCTI2C_OutCoord,&geCTMOS_CoordTempBuf,sizeof(__geCTI2C_OutCoord));
//
//	SFR_IE |= BIT7;//Enable All INT
//}


//***********************************************************************************************************************
//* Function: CTI2C_C_UpdateOSCoordBuff
//* Desc.: convert coord to OS buffer ( __geCTI2C_OutCoord )
//*			
//* Param:
//*		unsigned char MaxNumTouchs (input)
//*			max number of touchs
//*		unsigned char MaxNumKeys (input)
//*			max number of keys
//*		const unsigned char *pTouchState (input)
//*			pointer to pTouchState flag buffer
//*			0x00 = NO_TOUCH
//*			0x02 = TOUCH
//*			0x20 = TOUCH_UP (first NO_TOUCH)	
//*		const short *PosX (input)
//*			pointer to x position buffer
//*		const short *PosY (input)
//*			pointer to y position buffer
//* Return:
//*		NONE
//*	Usage Reg:
//*		
//* Stack:
//*		
//***********************************************************************************************************************
void CTI2C_C_UpdateOSCoordBuff(unsigned char MaxNumTouchs, unsigned char MaxNumKeys,const  unsigned char near *pTouchState,
							const  short near *PosX,const  short near *PosY)
{
	


	SFR_IE	&= ~BIT7;//Disable All INT
	

	//Convert To ST Protocol,  
	#if(REPLACE_I2C_PROTOCOL == REPLACE_I2C_SITRONIX_A)	 //output OS buffer = __geCTI2C_OutCoord
	{
		CTI2C_C_COORD_XY *pDataXY;
		unsigned char numTouch,i,key,b;
		short x,y;
			
		numTouch = 0;
		pDataXY	= &__geCTI2C_OutCoord.XyCoord0;
			
	 	do{

			if(((*pTouchState) & 0x0F) == 0)
			{//no touch
				pDataXY->XyHCoord &= 0x7F;	//set validate bit to 0
			}	
			else
			{//touch
				x = *PosX;
				y = *PosY;	
				pDataXY->XyHCoord = 0x80 | ((x&0xF00)>>4) | ((y&0xF00)>>8);
				pDataXY->XLCoord = x;
				pDataXY->YLCoord = y;
			}			
	
			if(pDataXY->XyHCoord & 0x80)
				numTouch++;

			pTouchState++;	
			pDataXY++;
			PosX++;
			PosY++;
			MaxNumTouchs--;
		}while(MaxNumTouchs != 0);
	
		__geCTI2C_OutCoord.Fingers =  numTouch;
	
		key = 0;
		b = 1;
		for(i = 0;i < MaxNumKeys;i ++)
		{
	
			if(*pTouchState & (0x0F))
			{
				key |= b;	
			}
			b <<= 1;
			pTouchState++;	
		}
	
	   	__geCTI2C_OutCoord.Keys =  key;

	}
	#endif

	SFR_IE |= BIT7;//Enable All INT
}


//;******************************************************************************
//;* Function: CTI2C_C_GetReplaceVer
//;* Desc.:  
//;* Param: 
//;*			input : None.
//;* Return:
//;*			output : unsigned char A, ReplaceVer.
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	I2C ISR
//;* Cycles:
//;*	
//;******************************************************************************
unsigned char CTI2C_C_GetReplaceVer(void)
{
	return	0x00;
}


//;******************************************************************************
//;* Function: CTI2C_C_GetDevePageCmd
//;* Desc.:  
//;* Param: 
//;*			input : None.
//;* Return:
//;*			output : unsigned char A, Development Page Command.
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	I2C ISR
//;* Cycles:
//;*	
//;******************************************************************************
unsigned char CTI2C_C_GetDevePageCmd(void)
{
	return	0xFF;
}

//;******************************************************************************
//;* Function: CTI2C_C_GetDevePageCmd
//;* Desc.:  
//;* Param: 
//;*			input : None.
//;* Return:
//;*			output : unsigned char A, Development Page Command Id Value.
//;* Used Register:
//;*		 	  
//;* Stack:
//;*	Used =  bytes.
//;*	Temp =  bytes.
//;* Notes:
//;*	I2C ISR
//;* Cycles:
//;*	
//;******************************************************************************
unsigned char CTI2C_C_GetDevePageIdValue(void)
{
	return 0xEF;
}


#endif//(CTMOS_I2C_REPL_C_IF==1)