@echo

SET /P VAR1=Please Input Fw Revision�G

REM ===============================================================================================================================================================
REM	====[Goodix Protocl]===========================================================================================================================================
REM ===============================================================================================================================================================

REM	====[Goodix Protocl KeyRxIndependent, Report Rate 1F1R]========================================================================================================
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			DEVICE_I2C_PROTOCOL				" "#define			DEVICE_I2C_PROTOCOL				DEVICE_I2C_GOODIX"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_KEY_RX_INDEPENDENT		" "#define			CTMOS_KEY_RX_INDEPENDENT		0x1"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COORD_REPORT_MODE			" "#define			CTMOS_COORD_REPORT_MODE			0x1"

call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_DEVELOP_ALL_DIS			" "#define			CTMOS_DEVELOP_ALL_DIS			0x1"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COMMAND_IO_READ_EN		" "#define			CTMOS_COMMAND_IO_READ_EN		0x0"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COMMAND_IO_WRITE_EN		" "#define			CTMOS_COMMAND_IO_WRITE_EN		0x0"
call:sub .\Code\McuOs\include\CTMOS_Config.h "	#define			CTMOS_SMART_WAKEUP_EN			" "	#define			CTMOS_SMART_WAKEUP_EN			0x2"

REM	==[ALGO_N_TOUCH_BAR_BAR]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_BAR_BAR"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYRX_BAR_1F1R_SWU.bin

REM	==[ALGO_N_TOUCH_CROSS_PATTERN]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_CROSS_PATTERN"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYRX_CROSS_1F1R_SWU.bin

REM	==[ALGO_N_TOUCH_1D_E_TYPE]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_1D_E_TYPE"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYRX_1DE_1F1R_SWU.bin

REM	====[Goodix Protocl KeyTxIndependent, Report Rate 1F1R]========================================================================================================
REM	call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			DEVICE_I2C_PROTOCOL				" "#define			DEVICE_I2C_PROTOCOL				DEVICE_I2C_GOODIX"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_KEY_RX_INDEPENDENT		" "#define			CTMOS_KEY_RX_INDEPENDENT		0x0"
REM call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COORD_REPORT_MODE			" "#define			CTMOS_COORD_REPORT_MODE			0x1"

REM	==[ALGO_N_TOUCH_BAR_BAR]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_BAR_BAR"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYTX_BAR_1F1R_SWU.bin

REM	==[ALGO_N_TOUCH_CROSS_PATTERN]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_CROSS_PATTERN"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYTX_CROSS_1F1R_SWU.bin

REM	==[ALGO_N_TOUCH_1D_E_TYPE]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_1D_E_TYPE"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYTX_1DE_1F1R_SWU.bin

REM	====[Goodix Protocl KeyRxIndependent, Report Rate 1F2R]========================================================================================================
REM call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			DEVICE_I2C_PROTOCOL				" "#define			DEVICE_I2C_PROTOCOL				DEVICE_I2C_GOODIX"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_KEY_RX_INDEPENDENT		" "#define			CTMOS_KEY_RX_INDEPENDENT		0x1"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COORD_REPORT_MODE			" "#define			CTMOS_COORD_REPORT_MODE			0x0"

REM	==[ALGO_N_TOUCH_BAR_BAR]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_BAR_BAR"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYRX_BAR_1F2R_SWU.bin

REM	==[ALGO_N_TOUCH_CROSS_PATTERN]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_CROSS_PATTERN"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYRX_CROSS_1F2R_SWU.bin

REM	==[ALGO_N_TOUCH_1D_E_TYPE]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_1D_E_TYPE"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYRX_1DE_1F2R_SWU.bin

REM	====[Goodix Protocl KeyTxIndependent, Report Rate 1F2R]========================================================================================================
REM	call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			DEVICE_I2C_PROTOCOL				" "#define			DEVICE_I2C_PROTOCOL				DEVICE_I2C_GOODIX"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_KEY_RX_INDEPENDENT		" "#define			CTMOS_KEY_RX_INDEPENDENT		0x0"
REM call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COORD_REPORT_MODE			" "#define			CTMOS_COORD_REPORT_MODE			0x0"

REM	==[ALGO_N_TOUCH_BAR_BAR]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_BAR_BAR"
call CTouch_Mcu.BAT
copy .\obj\CTouch_Mcu_chksum.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYTX_BAR_1F2R_SWU_S820TpCfgExtV25.bin
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYTX_BAR_1F2R_SWU.bin

REM	==[ALGO_N_TOUCH_CROSS_PATTERN]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_CROSS_PATTERN"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYTX_CROSS_1F2R_SWU.bin

REM	==[ALGO_N_TOUCH_1D_E_TYPE]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_1D_E_TYPE"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_GOODIX10_KEYTX_1DE_1F2R_SWU.bin


REM ===============================================================================================================================================================
REM	====[Sitronix Protocl]=========================================================================================================================================
REM ===============================================================================================================================================================

REM	====[Sitronix Protocl KeyRxIndependent, Report Rate 1F1R]======================================================================================================
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			DEVICE_I2C_PROTOCOL				" "#define			DEVICE_I2C_PROTOCOL				DEVICE_I2C_SITRONIX_A"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_KEY_RX_INDEPENDENT		" "#define			CTMOS_KEY_RX_INDEPENDENT		0x1"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COORD_REPORT_MODE			" "#define			CTMOS_COORD_REPORT_MODE			0x1"

REM	==[ALGO_N_TOUCH_BAR_BAR]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_BAR_BAR"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYRX_BAR_1F1R_SWU.bin

REM	==[ALGO_N_TOUCH_CROSS_PATTERN]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_CROSS_PATTERN"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYRX_CROSS_1F1R_SWU.bin

REM	==[ALGO_N_TOUCH_1D_E_TYPE]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_1D_E_TYPE"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYRX_1DE_1F1R_SWU.bin

REM	====[Sitronix Protocl KeyTxIndependent, Report Rate 1F1R]======================================================================================================
REM	call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			DEVICE_I2C_PROTOCOL				" "#define			DEVICE_I2C_PROTOCOL				DEVICE_I2C_SITRONIX_A"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_KEY_RX_INDEPENDENT		" "#define			CTMOS_KEY_RX_INDEPENDENT		0x0"
REM call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COORD_REPORT_MODE			" "#define			CTMOS_COORD_REPORT_MODE			0x1"

REM	==[ALGO_N_TOUCH_BAR_BAR]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_BAR_BAR"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYTX_BAR_1F1R_SWU.bin

REM	==[ALGO_N_TOUCH_CROSS_PATTERN]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_CROSS_PATTERN"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYTX_CROSS_1F1R_SWU.bin

REM	==[ALGO_N_TOUCH_1D_E_TYPE]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_1D_E_TYPE"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYTX_1DE_1F1R_SWU.bin

REM	====[Sitronix Protocl KeyRxIndependent, Report Rate 1F2R]======================================================================================================
REM call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			DEVICE_I2C_PROTOCOL				" "#define			DEVICE_I2C_PROTOCOL				DEVICE_I2C_SITRONIX_A"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_KEY_RX_INDEPENDENT		" "#define			CTMOS_KEY_RX_INDEPENDENT		0x1"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COORD_REPORT_MODE			" "#define			CTMOS_COORD_REPORT_MODE			0x0"

REM	==[ALGO_N_TOUCH_BAR_BAR]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_BAR_BAR"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYRX_BAR_1F2R_SWU.bin

REM	==[ALGO_N_TOUCH_CROSS_PATTERN]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_CROSS_PATTERN"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYRX_CROSS_1F2R_SWU.bin

REM	==[ALGO_N_TOUCH_1D_E_TYPE]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_1D_E_TYPE"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYRX_1DE_1F2R_SWU.bin

REM	====[Sitronix Protocl KeyTxIndependent, Report Rate 1F2R]======================================================================================================
REM	call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			DEVICE_I2C_PROTOCOL				" "#define			DEVICE_I2C_PROTOCOL				DEVICE_I2C_SITRONIX_A"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_KEY_RX_INDEPENDENT		" "#define			CTMOS_KEY_RX_INDEPENDENT		0x0"
REM call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COORD_REPORT_MODE			" "#define			CTMOS_COORD_REPORT_MODE			0x0"

REM	==[ALGO_N_TOUCH_BAR_BAR]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_BAR_BAR"
call CTouch_Mcu.BAT
copy .\obj\CTouch_Mcu_chksum.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYTX_BAR_1F2R_SWU_S820TpCfgExtV25.bin
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYTX_BAR_1F2R_SWU.bin

REM	==[ALGO_N_TOUCH_CROSS_PATTERN]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_CROSS_PATTERN"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYTX_CROSS_1F2R_SWU.bin

REM	==[ALGO_N_TOUCH_1D_E_TYPE]==
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			TOUCH_ALGO_ID					" "#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_1D_E_TYPE"
call CTouch_Mcu.BAT
SeparateBinProject_V1.0.exe .\obj\CTouch_Mcu_chksum.bin .\obj\CTouch_Mcu_chksum_fw.bin 0x3F00 .\obj\CTouch_Mcu_chksum_cfg.bin 0xFE
copy .\obj\CTouch_Mcu_chksum_fw.bin ..\A8008_NTouch_FW_Release_Bin\CSTOUCH_N_ST1633_A8008_%VAR1%_A10_KEYTX_1DE_1F2R_SWU.bin

call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_DEVELOP_ALL_DIS			" "#define			CTMOS_DEVELOP_ALL_DIS			0x0"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COMMAND_IO_READ_EN		" "#define			CTMOS_COMMAND_IO_READ_EN		0x1"
call:sub .\Code\McuOs\include\CTMOS_Config.h "#define			CTMOS_COMMAND_IO_WRITE_EN		" "#define			CTMOS_COMMAND_IO_WRITE_EN		0x1"
call:sub .\Code\McuOs\include\CTMOS_Config.h "	#define			CTMOS_SMART_WAKEUP_EN			" "	#define			CTMOS_SMART_WAKEUP_EN			0x0"

del .\obj\CTouch_Mcu_chksum_fw.bin
del .\obj\CTouch_Mcu_chksum_cfg.bin

pause
exit

REM �Ƶ{��
:sub
for /f "tokens=1* delims=:" %%i in ('findstr /n ".*" %1') do (
    if "%%j"=="" (echo.>>tmp) else (
          echo %%j|find %2>nul&&(call echo %~3>>tmp)||(echo %%j>>tmp)
    )
)
copy tmp %1 /y >nul&&del tmp
