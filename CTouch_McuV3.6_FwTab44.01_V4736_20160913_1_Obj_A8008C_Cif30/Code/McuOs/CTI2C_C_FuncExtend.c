//#include "include\CTMOS_Config.h"
#include "include\A8008_C_Def.h"
//#include "include\CTMOS_DATA.h"
#include "include\CTI2C_C_FuncTable_Def.h"
#include "include\CTI2C_C_FuncExtend.h"
#include <math.h>

//#if(CTMOS_I2C_REPL_C_IF==1)
//;===========================================================================
//; 变量定义
//;===========================================================================
UINT8  edata point_data[4];
UINT8  Iic_rev_buf[32];					//I2C接收数据的长度BUF
touch_struct  edata  touch;
//MULIT_TOUCH   edata  mtouch;
UINT8  bdata  system_boll_bit  	_at_ 0x2a;
UINT8  bdata  tp_bool_bit		_at_ 0x2b;
UINT8  bdata  key_bool_bit		_at_ 0x2c;
UINT8  bdata  hid_bool_bit     	_at_ 0x2d;
//UINT8  bdata  i2c_bool_bit		_at_ 0x2e;
//----------------------------------
//HID_I2C部分
//----------------------------------
UINT32	mouse_timer_couter;			//定义32位是为了防止溢出
UINT16	mouse_timer_buf;				//用于填写时间
UINT8	set_report_data_len;				//收到数据的长度
UINT8	cmd_id_data;					//主机发送的ID号
UINT16	set_report_cnt;					//SET REPORT的读取数据计数

UINT8	hid_data_buf;					//ID的暂存和命令的暂存
UINT8	hid_status_cmd;					//HID接收HOST发送的命令状态
UINT8	send_hid_data_cout;				//发送数据多少次的计数
//----------------------------------
//TOUCH处理部分
//----------------------------------
UINT8	touch_up_cout;					//有提笔的坐标位置
UINT8	touch_have_data;				//有有效数据的坐标位置
UINT8 	old_touch_have_data;			//上次有效的触摸位置
UINT8	HID_send_status;				//HID发送的状态
UINT8	finger_num_buf;					//此次上报手指的个数

UINT8	finger_cout_data[5];				//第几个手指，最大4个
UINT8	finger_cout_status[5];			//每个坐标的状态描述
UINT8	need_coord_flag;				//需要发送坐标的位置
UINT8	vail_touch_count;

//----------------------------------
//使能和功能部分标志
//----------------------------------

bit int_flag;
bit send_reset_flag;
bit ready_reset_flag;
bit recv_reset_flag;



//bit  buf_mem1;
//bit  buf_mem2;
//bit  fiic_inv;
//bit  fpalm_set;
//bit  fkey_send;

/*
sbit  no_send_mouse_data_flag  = i2c_bool_bit^0;
sbit  first_ptp_flag      = i2c_bool_bit^1;
sbit  int_flag 			= i2c_bool_bit^2;
sbit  buf_mem1 	  	= i2c_bool_bit^3;
sbit  buf_mem2       	= i2c_bool_bit^4;
sbit  fiic_inv 			= i2c_bool_bit^5;
sbit  fpalm_set		= i2c_bool_bit^6;
sbit  fkey_send        	= i2c_bool_bit^7;
*/

sbit  mouse_mode_flag			= hid_bool_bit^0;	// 1为PTP，0为普通
sbit  buf_mem1					= hid_bool_bit^1;
sbit  buf_mem2					= hid_bool_bit^2;
sbit  no_send_mouse_data_flag	= hid_bool_bit^3;	//0表示可以发送，1表示不能发送			
sbit  fiic_inv					= hid_bool_bit^4;
sbit  first_ptp_flag			= hid_bool_bit^5;	//打开计数存储器计数
sbit  set_report_rec_end_flag	= hid_bool_bit^6;	//SET REPORT接收完毕HOST发送的数据
sbit  init_hid_des_flag			= hid_bool_bit^7;	//初始化完成标志


sbit  send_need       		= system_boll_bit^0;
sbit  mouse_send      		= system_boll_bit^1;
sbit  status_key_full 		= system_boll_bit^2;
sbit  mid_valid 	  		= system_boll_bit^3;
sbit  free_send       		= system_boll_bit^4;
sbit  fsys_powerdown  		= system_boll_bit^5;
sbit  fsys_rest		  		= system_boll_bit^6;
sbit  fint_low        		= system_boll_bit^7;

sbit  status_drag   		=tp_bool_bit^0;
sbit  status_key    		=tp_bool_bit^1;
sbit  status_move   		=tp_bool_bit^2;
sbit  ftouch_sta    		=tp_bool_bit^3;
sbit  fgest_out     		=tp_bool_bit^4;
sbit  froll_set     		=tp_bool_bit^5;
sbit  fkey_hit_i    		=tp_bool_bit^6;
sbit  fpalm_set				=tp_bool_bit^7;

sbit  key_line_M    		=key_bool_bit^0;
sbit  key_check_str 		=key_bool_bit^1;
sbit  key_check    			=key_bool_bit^2;
sbit  fkey_inv 				=key_bool_bit^3;
sbit  mid_key_L     		=key_bool_bit^4;
sbit  mid_key_R	    		=key_bool_bit^5;
sbit  fi2c_str     			=key_bool_bit^6;
sbit	 fkey_send			=key_bool_bit^7;


uint8 int_i2c_ct,int_houd_timovl_ct;
bit fpalm_set_up;

bit delay_allow_read_flag = 0;
uint16 delay_allow_read_cout = 0;

//;===========================================================================
//; DEFINE
//;===========================================================================
//==[STRUCT]==
typedef struct
{
	unsigned char XyHCoord;//XyHCoord[7]=Valid : XyHCoord[6:4]=X_High : XyHCoord[3]=Reserved : XyHCoord[2:0]=Y_High
	unsigned char XLCoord;
	unsigned char YLCoord;
	unsigned char Reserved0;
}CTI2C_C_COORD_XY;

typedef struct
{
	unsigned char Fingers;
	unsigned char Keys;
	CTI2C_C_COORD_XY XyCoord0;
	CTI2C_C_COORD_XY XyCoord1;
	CTI2C_C_COORD_XY XyCoord2;
	CTI2C_C_COORD_XY XyCoord3;
	CTI2C_C_COORD_XY XyCoord4;
}CTI2C_C_ST_COORD;

//;===========================================================================
//; EXTERN
//;===========================================================================
extern	void CTI2C_C_IntSignalLow(void);
extern	void CTI2C_C_IntSignalHigh(void);


extern unsigned char edata	getCTI2C_RegMapMem[];
extern bit gbtCTMOS_I2cCoordBufFullFlag;//With StCoord Flag, Set By Algo, Clr By Os 
extern short data gtCTMOS_I2cResX;
extern short data gtCTMOS_I2cResY;
extern unsigned char data gtCTI2C_CmdAddr;
extern unsigned char data gtCTI2C_ByteCnt;
//extern unsigned char edata geCTMOS_CoordTempBuf[];//StCoord
extern bit gbCTAP_TouchPalmFlag;//0->Do Nothing, 1->With Touch Palm

CTI2C_C_ST_COORD edata	__geCTI2C_OutCoord;

extern unsigned char edata geCTTMX_IdleCaliCnt;//Counting For Calibration, Unit:Frame
//Example: Idle Frame Period = 100ms(10Hz), geCTTMX_IdleCaliCnt = 50
//  Calibration Period = Idle Frame Period * geCTTMX_IdleCaliCnt
// 	Calibration Period = 100 * 50 = 5000ms

extern unsigned char edata geCTMOS_TimerxCntTrimValue;//2msUnit
extern unsigned char edata geCTMOS_TimerxCntSwkIdleValue;//Idle Cnt(Up Count)
//Example 1:IdlePeriodCnt:15Hz->66ms/2ms=33,10Hz->100ms/2ms=50,5Hz->200ms/2ms=100
//Example 2:
//	temp = ((0x100-geCTMOS_TimerxCntTrimValue)*(IdlePeriodCnt))/64);
//	geCTMOS_TimerxCntSwkIdleValue = 0x100 - temp; 
//PS1 : temp Can't Over Flow;
//PS2 : geCTMOS_TimerxCntSwkIdleValue is Zero than Idle To slowest;

extern bit gbtCTSCAN_SetScanActiveFlag;//0->Do Nothing, 1->Idle to Active Scan Mode
//---------------------------------------------//
//函数部分
//--------------------------------------------//
unsigned char CTI2C_C_GetReplaceVer(void)
{
	return	0x00;
}

unsigned char CTI2C_C_GetDevePageCmd(void)
{
	return	0xFF;
}

unsigned char CTI2C_C_GetDevePageIdValue(void)
{
	return 0xEF;
}
void CTI2C_C_WrCmdNoParam(void)
{

}
void CTI2C_C_WrStopIsr(void)
{
}
void CTI2C_C_IntSignalCtrlHostRdCoordEvent(void)
{	

}

//**********************************************************************************************************
//* Function: CTI2C_C_MemCopy                                                                                    
//* Description:                                                                                            
//*      memory copy                                                     
//*                                                                                                             
//* Parameters:                                                                                             
//*		void *dest (input)                                                                                               
//*      	pointer to destination buffer   
//*		void *src (input)                                                                                              
//*      	pointer to source buffer
//*		unsigned int len (input)
//*			buffer size in bytes                                                                                 
//* Returns:                                                                                                
//*      None                                                                                                                                                                                             
//**********************************************************************************************************
/*
void CTI2C_C_MemCopy(void near *dest, void near *src, unsigned char len)
{
	unsigned char near *pDest = dest;
	unsigned char near *pSrc = src;
	if(len == 0) return;
	do
	{
		*pDest++ = *pSrc++;
	 	len--;
	}while(len != 0);
}
*/
//;******************************************************************************
//;* Function: CTI2C_IntSignalCtrlTrigHostRdCoordEvent
//;******************************************************************************
void CTI2C_C_IntSignalCtrlTrigHostRdCoordEvent(void)
{
	if(!no_send_mouse_data_flag)				//如果系统禁止数据发送，直接不读取坐标
	{
 		if(!mouse_send && init_hid_des_flag)
  			mtcp_coord();
	}
}

//;******************************************************************************
//;复位
//;******************************************************************************
bit CTI2C_C_OsGetDeviceCtrlReset(void)
{
	if(fsys_rest)
   		return 1;
   	else
   		return	0;
}
//;******************************************************************************
//;* 休眠
//;******************************************************************************
bit CTI2C_C_OsGetDeviceCtrlPwrDn(void)
{
	if(fsys_powerdown)
	{
		  INT_O =1; //加入更新有触摸POWERDOWN不能休眠
		  P12 =1;
		  //SFR_P1DIR |= 0x03;  //输入
		  //SFR_AIE |= WAKE_EXIO_BIT;//1->enable Exio Interrupt
		  #if 0
		  if(!P11)
		  {
		  	 fsys_powerdown =0;
			 SFR_P1DIR &= ~0x01;	//输出
			 SFR_AIE &= ~WAKE_EXIO_BIT;//1->disable Exio Interrupt
			 return	0;
		  }
		  #endif
		  return 1;   //power down
	}
	else             //wake up
	{
		return 0;
	}

}
//;******************************************************************************
//;* 初始化
//;******************************************************************************
void CTI2C_C_FuncExtendIni(void)
{
	//UINT16 i;
    SFR_P1DIR = 0X05;		//P10输出，P11输入
	SFR_P1    = 0x7; 
	SFR_P1PU  = 0x7;

	//Set interrupt
	SFR_EGTG1 &= ~WAKE_UP_PIN;//0->Falling Edge
	SFR_EGFG1 &= ~WAKE_UP_PIN;//0->Clr Request Flag
	SFR_EGEN1 |= WAKE_UP_PIN;//1->Enable EXIO
	SFR_AIF &= ~WAKE_EXIO_BIT;//0->Clr Exio Request Flag
	SFR_AIE &= ~WAKE_EXIO_BIT;//1->disable Exio Interrupt
	//Set NMI interrupt
	SFR_NMIE |= WAKE_EXIO_BIT;//1->Enable NMI Exio Interrupt

	//SFR_P1DIR &= ~0x03;	//输出
	INT_O =0;
	
	inti_reg_data();
	
	touch_rece_free();
	INT_O =1;
	//touch.resy_mid =((gtCTMOS_I2cResY<<1)*13);//(13/16)
}
//--------------------------------
//初始化寄存器和标志
//--------------------------------
void inti_reg_data(void)
{
	UINT16 i;
	gtCTI2C_CmdAddr = 0x00;
	for(i=0;i<256;i++)
	{
		getCTI2C_RegMapMem[i]= 0x00;		//数据长度低8位
	}
	for(i=0;i<5;i++)
	{
		finger_cout_data[i] = 0;
		finger_cout_status[i] = 0;
	}
	
	touch_up_cout = 0;
	touch_have_data = 0;
	old_touch_have_data = 0;
	HID_send_status = 0;
	finger_num_buf = 0;
	need_coord_flag = 0;

	int_flag = 0;
	buf_mem1 =0;
	buf_mem2 =0;
	fiic_inv = 0;
	system_boll_bit =0;
	key_bool_bit =0;
	hid_bool_bit = 0;

	mouse_timer_couter = 0;
	mouse_timer_buf = 0;
	set_report_data_len = 0;		//set report的有效数据长度
	cmd_id_data = 0;
	set_report_cnt = 0;
	
	hid_data_buf= 0;
	hid_status_cmd = no_cmd;
	send_hid_data_cout = 0;
	vail_touch_count = 0;

	send_reset_flag = 0;
	ready_reset_flag = 0;
	recv_reset_flag = 0;

	delay_allow_read_flag = 0;
	delay_allow_read_cout = 0;
}


//;******************************************************************************
//;* 根据命令发送I2C数据
//;*			input :gtCTI2C_CmdAddr. 
//;* Return:
//;*			output :C, 0-> Read Coord Command.
//;*					   1-> Not Read Coord Command. 
//;******************************************************************************
bit CTI2C_C_HostRdCmdParser(void)
{
	uint16 i;

    gtCTI2C_ByteCnt =0;
	int_houd_timovl_ct =0;
	gtCTI2C_CmdAddr = 0x00;
	#if 1
	if(HID_send_status == SEND_HID_DES_CMD)			//获取HID描述时
 	{
 		send_hid_data_cout = 0;
		HID_send_status = SEND_NUTI_HID_CMD;
		for(i=0;i<8;i++)			//装载30个BYTE
		{
			getCTI2C_RegMapMem[i] = HID_Descript[i];
		}
		send_hid_data_cout++;
		return 1;
 	}
	else if(HID_send_status == SEND_REPORT_DES_CMD)	//如果是报告发送标志
	{

		
	   #if 0
		HID_send_status = 0;
		
		gtCTI2C_CmdAddr= 0x00;	//设置首地址为0
		for(i=0;i<256;i++)			//装载前面256个字节
		{
			getCTI2C_RegMapMem[i] = Report_Descript[i];
		}
		REPORT_send_two_flag = 1;
	   #else
	   	send_hid_data_cout = 0;
	  	HID_send_status = SEND_MUTI_REPORT_CMD;		//需要多次发送
		for(i=0;i<8;i++)			//装载前面8个字节
		{
			getCTI2C_RegMapMem[i] = Report_Descript[i];
		}
		send_hid_data_cout++;
	   #endif
		return 1;
	}
	else if(hid_status_cmd == get_report_cmd) 	//如果是GET REPORT
	{
		init_hid_des_flag  = 1;
		hid_status_cmd = no_cmd;
		if(cmd_id_data == CONTACT_MAXIMUM_REPORT)
		{
			getCTI2C_RegMapMem[0x00]= 0x04;		//数据长度低8位
			getCTI2C_RegMapMem[0x01]= 0x00;		//数据长度高8位
			getCTI2C_RegMapMem[0x02]= CONTACT_MAXIMUM_REPORT;		//ID号
			getCTI2C_RegMapMem[0x03]= 0x05;		//手指的个数低8位
			//getCTI2C_RegMapMem[0x04]= 0x00;		//手指的个数高8位
		}
		else if(cmd_id_data == Vendor_Defined_1)
		{
		    #if 0
			gtCTI2C_CmdAddr = 0x00;
			getCTI2C_RegMapMem[0x00]= 0x03;		//数据长度低8位
			getCTI2C_RegMapMem[0x01]= 0x01;		//数据长度高8位
		    
			for(i=0;i<254;i++)
			{
				getCTI2C_RegMapMem[0x02+i] = PTP_Vendor[i];	//ID已经包含在里面
			}
			get_feture_two_flag = 1;
		    #else
			send_hid_data_cout = 0;
			HID_send_status = SEND_MUTI_VENDOR_CMD;
			for(i=0;i<8;i++)
			{
				getCTI2C_RegMapMem[i] = PTP_Vendor[i];
			}
			send_hid_data_cout++;
		    #endif
		}
		cmd_id_data = 0;
		return 1;
	}
	#if 0
	else if((hid_status_cmd == reset_rebuf_cmd)&& (HID_send_status== 0))	//reset return BUF
	{
		hid_status_cmd = no_cmd;
		for(i=0;i<256;i++)
		{
			getCTI2C_RegMapMem[i]= 0x00;		//数据长度低8位
		}
		fint_low =0;
		INT_O =1;
		//P11 =1;
		return 1;
	}
	#endif
	else if(send_reset_flag == 1)
	{
		send_reset_flag = 0;
		gtCTI2C_CmdAddr = 0x00;
		for(i=0;i<256;i++)
		{
			getCTI2C_RegMapMem[i]= 0x00;		//数据长度低8位
		}
		fint_low =0;
		return 1;
	}
	else if(hid_status_cmd == get_idle_cmd)		//GET IDLE
	{
		hid_status_cmd = no_cmd;
		getCTI2C_RegMapMem[0x00]= 0x04;		//数据长度低8位
		getCTI2C_RegMapMem[0x01]= 0x00;		//数据长度高8位
		getCTI2C_RegMapMem[0x02]= cmd_id_data;	//ID号
		getCTI2C_RegMapMem[0x03]= 0x05;		//手指的个数低8位
		//getCTI2C_RegMapMem[0x04]= 0x00;		//手指的个数高8位
		return 1;
	}
	else if(hid_status_cmd == get_protocol_cmd)	//GET PROTOCOL
	{
		hid_status_cmd = no_cmd;
		getCTI2C_RegMapMem[0x00]= 0x04;		//数据长度低8位
		getCTI2C_RegMapMem[0x01]= 0x00;		//数据长度高8位
		getCTI2C_RegMapMem[0x02]= 0x00;		//ID号
		getCTI2C_RegMapMem[0x03]= 0x05;		//手指的个数低8位
		//getCTI2C_RegMapMem[0x04]= 0x00;		//手指的个数高8位
		return 1;
	}
 	
	#else
	if(gtCTI2C_CmdAddr!=0xff)	 
		gtCTI2C_CmdAddr= 0x10;
	
 	if(gtCTI2C_CmdAddr ==0x10)
	#endif
	else if((HID_send_status == 0) && (hid_status_cmd == 0))
 	{
 		//gtCTI2C_CmdAddr= 0x00;
		//gtCTI2C_ByteCnt = 0;
 	    if(fint_low)
 		{
 		 	fi2c_str =1;
			#if 0
         		touch_filter_sub();
			#endif
			send_hid_data_cout = 0;
			HID_send_status = SEND_MUTI_DATA_CMD;
 		 	for(i=0;i<8;i++)
 		   	{
 		    	getCTI2C_RegMapMem[i]= iic_buf[i];
				iic_buf[i] =0;
 		   	}
			send_hid_data_cout++;
			#if 0
		 	buf_mem1 =0;
		 	if(buf_mem2)
		 	{
	 	 	 	for(i=0;i<IN_DATA_LENG;i++)
	 		   	{
	 		    		iic_buf[i]= iic_buf[i+IN_DATA_LENG];
					iic_buf[i+IN_DATA_LENG] =0;
	 		   	}
			 	buf_mem2 =0;
			 	buf_mem1 =1;
		 	}
			#endif
 		}
	   	else 
	   	{
	   		#if 1
			getCTI2C_RegMapMem[0x00] =0;
			for(i=0;i<IN_DATA_LENG;i++)			//将所有数据清0
			{
				getCTI2C_RegMapMem[0x00+i] = 0;
			}
			#else
			for(i=0;i<16;i++)			//将所有数据清0
			{
				getCTI2C_RegMapMem[0x10+i] = 0;
			}
			for(i=2;i<=8;i+2)			//在第3，5，7，9个数据中填0x55，用于POLING判断
			{
				getCTI2C_RegMapMem[0x10+i] = 0x55;
			}
			#endif
	   	}
		
	   	return 1;
 	}
	else
	{
		HID_send_status = 0;
		HID_send_status = 0;
		send_hid_data_cout = 0;
		return 1;
	}
}
//;******************************************************************************
//;*字节为空，需要新加载数据
//;*			input : gtCTI2C_CmdAddr.
//;*			input : gtCTI2C_ByteCnt.
//;******************************************************************************
void CTI2C_C_BufEmpIsr(void)
{
	UINT8 i;
	UINT16 tempi= 0;
	gtCTI2C_ByteCnt =0;
	gtCTI2C_CmdAddr = 0;
	tempi = send_hid_data_cout*8;	//计算当前待发送数据地址偏移
	if(HID_send_status == SEND_MUTI_REPORT_CMD)
	{
		if(send_hid_data_cout < 75)    //0-71,72次，72*8=576
		{
			for(i=0;i<8;i++)
			{
				getCTI2C_RegMapMem[i] = Report_Descript[tempi+i];
			}
			send_hid_data_cout++;
		}
		else if(send_hid_data_cout == 75)
		{
			#if 1
			for(i=0;i<3;i++)
			{
				getCTI2C_RegMapMem[i] = Report_Descript[tempi+i];
			}
			#endif
			//getCTI2C_RegMapMem[0] = Report_Descript[tempi];
			send_hid_data_cout = 0;
			HID_send_status = 0;
			
			init_hid_des_flag = 1;

			if(recv_reset_flag == 1)
			{
				recv_reset_flag = 0;
				ready_reset_flag = 1;
			}
		}
	}
	else if(HID_send_status == SEND_MUTI_VENDOR_CMD)
	{
		//if(send_hid_data_cout < 32)    //0-31,32次，32*8=256
		{
			for(i=0;i<8;i++)
			{
				getCTI2C_RegMapMem[i] = PTP_Vendor[tempi+i];
			}
			send_hid_data_cout++;
		}
		#if 0
		else if(send_hid_data_cout == 32)
		{
			for(i=0;i<3;i++)
			{
				getCTI2C_RegMapMem[i] = PTP_Vendor[tempi+i];
			}
			send_hid_data_cout = 0;
			HID_send_status = 0;
		}
		#endif
	}
	else if(HID_send_status == SEND_NUTI_HID_CMD)
	{
		//if(send_hid_data_cout<3)		//0-2,3次，24
		{
			for(i=0;i<8;i++)
			{
				getCTI2C_RegMapMem[i] = HID_Descript[tempi+i];
			}
			send_hid_data_cout++;
		}
		#if 0
		else if(send_hid_data_cout == 3)
		{
			for(i=0;i<6;i++)
			{
				getCTI2C_RegMapMem[i] = HID_Descript[tempi+i];
			}
			send_hid_data_cout = 0;
			HID_send_status = 0;
		}
		#endif
	}
	else if(HID_send_status == SEND_MUTI_DATA_CMD)
	{
		for(i=0;i<8;i++)
		{
			getCTI2C_RegMapMem[i] = iic_buf[tempi+i];
			iic_buf[tempi+i] = 0;
		}
		if(send_hid_data_cout<3)		//0-2,3次，24
		{
			/*
			for(i=0;i<8;i++)
			{
				getCTI2C_RegMapMem[i] = iic_buf[tempi+i];
				iic_buf[tempi+i] = 0;
			}
			*/
			send_hid_data_cout++;
		}
		#if 1
		else if(send_hid_data_cout==3)
		{
			/*
			for(i=0;i<8;i++)
			{
				getCTI2C_RegMapMem[i] = iic_buf[tempi+i];
				iic_buf[tempi+i] = 0;
			}
			*/
			send_hid_data_cout = 0;
			HID_send_status = 0;
			buf_mem1 =0;
			if(buf_mem2)
			{
		 		for(i=0;i<IN_DATA_LENG;i++)
		   		{
		    		iic_buf[i]= iic_buf[i+IN_DATA_LENG];
					iic_buf[i+IN_DATA_LENG] =0;
		   		}
		 		buf_mem2 =0;
		 		buf_mem1 =1;
			}
		}
		#endif
	}
	//send_hid_data_cout++;
}
//;******************************************************************************
//;* Function: I2C停止标志
//;* Desc.:  
//;* Param: 
//;*			input : gtCTI2C_CmdAddr.
//;*			input : gtCTI2C_ByteCnt.
//;******************************************************************************
void CTI2C_C_RdStopIsr(void)
{
	//UINT8 i;
	if((HID_send_status == SEND_NUTI_HID_CMD) || (HID_send_status == SEND_MUTI_VENDOR_CMD))
	{
		HID_send_status = 0;
		send_hid_data_cout = 0;
	}
	#if 0
	if(HID_send_status == SEND_MUTI_DATA_CMD)
	{
		HID_send_status = 0;
		send_hid_data_cout = 0;
		buf_mem1 =0;
		if(buf_mem2)
		{
			for(i=0;i<IN_DATA_LENG;i++)
			{
		    		iic_buf[i]= iic_buf[i+IN_DATA_LENG];
				iic_buf[i+IN_DATA_LENG] =0;
		   	}
		 	buf_mem2 =0;
		 	buf_mem1 =1;
		}
	}
	#endif
	if(fi2c_str)
	{
	 	INT_O =1;
	 	//P11 =1;
	 	fi2c_str =0;
	 	fint_low =0;
	 	int_i2c_ct =0;
		fiic_inv = 1;
	}
}
//;******************************************************************************
//;HOST写数据，接收数据及判断
//;******************************************************************************
bit CTI2C_C_WrCmdRangeCk(unsigned char Param0)
{	
	UINT16 TEMP_DATA=0;
	UINT16 i;
    fsys_powerdown =0;
	fsys_rest =0;
	int_houd_timovl_ct =0;
	SFR_P1DIR &= ~0x02;	//输出
	SFR_AIE &= ~WAKE_EXIO_BIT;//1->disable Exio Interrupt

    	if(gtCTI2C_CmdAddr ==0xc0)
    	{
    	 	if(!gtCTI2C_ByteCnt)
		{
			 if(!Param0)
			 	fsys_powerdown =1;
			 else if(Param0==0x5f)
			 	fsys_rest =1;
		 	 return 1;
		 }
    	}
	#if 1
	else if(gtCTI2C_CmdAddr == HID_DES_ADDR)	//发送HID，30BYTE
	{
		if(!gtCTI2C_ByteCnt)
		{
			HID_send_status = SEND_HID_DES_CMD;
			return 1;
		}
	}
	else if(gtCTI2C_CmdAddr == HID_REG_ADDR)	//发送REPORT，577BYTE
	{
		if(!gtCTI2C_ByteCnt)
		{
			HID_send_status = SEND_REPORT_DES_CMD;
			return 1;
		}
	}
	else if(gtCTI2C_CmdAddr == CMD_REG_ADDR)	//发送命令数据
	{
		if(gtCTI2C_ByteCnt == 1)					//BYTE2
		{
			if((Param0 & 0xf0) == 0x30) 			//FETURE CMD
			{
				HID_send_status = SEND_FETURE_CMD;	//则表示FETURECMD
				cmd_id_data = Param0 & 0x0f;	//保存CMD的ID
			}
			else
			{
				hid_data_buf = Param0;
			}
		}
		else if(gtCTI2C_ByteCnt == 2)			//byte3，
		{
			if(HID_send_status == SEND_FETURE_CMD)	//如果是feture的CMD
			{
				HID_send_status = 0;
				if(Param0 == 0x03)
				{
					hid_status_cmd = set_report_cmd;	//是SET FETURE
				}
				else if(Param0 == 0x02)
				{
					hid_status_cmd = get_report_cmd;	//是GET FETURE
				}
				else if(Param0 == 0x04)
				{
					hid_status_cmd = get_idle_cmd;		//GET IDLE
				}
				else if(Param0 == 0x05)
				{
					hid_status_cmd = set_idle_cmd;		//set idle
				}
				else if(Param0 == 0x06)
				{
					hid_status_cmd = get_protocol_cmd;		//get protocol
				}
				else if(Param0 == 0x07)
				{
					hid_status_cmd = set_protocol_cmd;		//set protocol
				}
				#if 0
				else if(Param0 == 0x0e)
				{

				}
				else
				{

				}
				#endif
			}
			else if(Param0 == 0x01)		//复位CMD
			{
		 		//INT_O =0;
		 		//P11 =0;
			#if 1
				//hid_status_cmd = reset_cmd;	//复位标志置1
				recv_reset_flag = 1;
				gtCTI2C_CmdAddr = 0x00;
				for(i=0;i<256;i++)
				{
					getCTI2C_RegMapMem[i]= 0x00;		//数据长度低8位
				}
			#else
				fsys_rest = 1;
			#endif
			}
			#if 1
			else if(Param0 == 0xfe)
			{
				fsys_rest = 1;
			}
			#endif
			else if(Param0 == 0x08)		//power set
			{
				if(hid_data_buf == 0x00)
				{
					fsys_powerdown = 0;
				}
				else if(hid_data_buf == 0x01)
				{
					fsys_powerdown = 1;
				}
			}
		}
		#if 1
		if(hid_status_cmd == set_report_cmd)	//如果是SET REPORT
		{
			if(gtCTI2C_ByteCnt == 5)			//5,6分别获取SET FETURE的数据长度
			{
				set_report_data_len = Param0;
			}
			else if(gtCTI2C_ByteCnt == 6)		//长度高位
			{
				TEMP_DATA = Param0;
				set_report_data_len |= (TEMP_DATA<<8);
				set_report_data_len = set_report_data_len - 2;
			}
			else if(gtCTI2C_ByteCnt >= 7)		//实际数据保存
			{
				Iic_rev_buf[set_report_cnt] = Param0;
				set_report_cnt++;
				if(set_report_cnt == set_report_data_len)
				{
					set_report_cnt = 0;
					set_report_rec_end_flag = 1;
					hid_status_cmd = no_cmd;
				}
			}
			if(set_report_rec_end_flag == 1)
			{
				set_report_rec_end_flag = 0;
				if(Iic_rev_buf[0] == FINGER_FATURE_REPORT)	//如果ID是
				{
					if(Iic_rev_buf[1] == 0x00)					//发的值为0
					{
						no_send_mouse_data_flag = 1;		//不允许发送数据
					}
					else if(Iic_rev_buf[1] == 0x03)			//发的值为3
					{
						no_send_mouse_data_flag= 0;		//允许发送数据
					}
					init_hid_des_flag = 1;					//初始化完成
				}
				else if(Iic_rev_buf[0] == DEVICE_MODE_REPORT)	//如果是设置模式
				{
					if(Iic_rev_buf[1] == 0x00)					//发的值为0
					{
						mouse_mode_flag = 0;				//普通鼠标模式
					}
					else if(Iic_rev_buf[1] == 0x03)			//发的值为3
					{
						mouse_mode_flag = 1;				//PTP模式
					}
					init_hid_des_flag = 1;
				}
				else if(Iic_rev_buf[0] == Vendor_Defined_2)		//如果是设置模式
				{
					init_hid_des_flag = 1;
				}
				else if(Iic_rev_buf[0] == Driver_Vendor_Defined)//如果是09，发送自定义的休眠命令
				{
					if(Iic_rev_buf[1] == 0xc0)					//如果值为C0，则表示为发送休眠命令，需要休眠
					{
						fsys_powerdown = 1;
					}
					else if(Iic_rev_buf[1] == 0xf5)	
					{
						fsys_rest = 1;
					}
				}
			}
		}
		#endif
	}
	#endif
	return 0;
}


//;******************************************************************************
//;* Function: 主函数
//;*	Main Loop
////==[Example]===============
////Ex:15Hz->66ms/2ms=33,10Hz->100ms/2ms=50,5Hz->200ms/2ms=100,2Hz->500ms/2ms=250
//	unsigned short temp1;
//	temp1 =	(((0x100-geCTMOS_TimerxCntTrimValue)*50)/64);//Ex:10Hz
//	if(temp1>256)
//		temp1=256;
//
//	geCTMOS_TimerxCntSwkIdleValue = (unsigned char)(0x100 - temp1);
//
////==[Example]===============
//// 	Calibration Period = 100ms * 160 = 16000ms
//	geCTTMX_IdleCaliCnt = 40;//160;
//
////==[Example]===============
//	gbtCTSCAN_SetScanActiveFlag=1;
//;******************************************************************************
void CTI2C_C_FuncExtendMain(void)
{
	//UINT16 i;
 	if(!fsys_powerdown)//加入更新有触摸POWERDOWN不能休眠
 	{
 		#if 1
 		if(ready_reset_flag == 1)
 		{
 			ready_reset_flag = 0;
			CTI2C_C_FuncExtendIni();
			//fint_low =1;
		 	//INT_O =0;
			send_reset_flag = 1;
 		}
 		#endif
 		//if(init_hid_des_flag)
 		{
		  	if(int_flag)
			  	int_count_check();
		  	if(fkey_inv)
			  	key_loop();
		  	if(buf_mem1 && !fint_low )//从stopisr移到这里因为，不能扫描key_loop
			{
			 	fint_low =1;
			 	INT_O =0;
			}
 		
	  		//if(fiic_inv && mouse_send)
	  		if(mouse_send)// && init_hid_des_flag)
		 		TOUCH_TRAN();
			//if(delay_allow_read_flag)
				//delay_allow_read_flag = 0;
 		}
 	}

}



//;******************************************************************************
//;* 定时器2MS
//;******************************************************************************
void CTPWR_C_2msTimerxIsr(void)
{
	static UINT8 edata int_10ms_ct,int_key_inv,palm_ct,palm_delay_ct;
	//UINT16 i;
	SFR_CLKWM = 0;
	if(++int_10ms_ct>=5)
	{
	  	int_10ms_ct =0;
	  	int_flag =1; 
	  	fkey_inv =1;
      	if(++int_houd_timovl_ct>250)
	 	{
			SFR_I2CADR=SFR_I2CADR&0x7f;
			int_houd_timovl_ct =0;
			SFR_I2CADR=SFR_I2CADR|0x80;
	 	}
	}
	if(fint_low)
	{
	  	if(++int_i2c_ct>=100)
		{
		  	fint_low =0;
		  	INT_O =1;
		  	int_i2c_ct =0;
		 }
	 }
	 else 
	 {
	 	int_i2c_ct =0;
	 }
	#if 0
  	if(!fiic_inv)
	{
	 	if(++iic_inv_ct>=3)
		{
		 	iic_inv_ct =0;
		 	//fiic_inv =1;
		 	if(fpalm_set)
		 		mouse_send =1;
			#if 0
		 	if(key_line_M)
		 	{
		 	 	if(!ftouch_sta)
		 	 	{
		 	 	 	fkey_send =1;
			 	 	mouse_send =1;
		 	 	}
		 	}
		 	
		 	else if(fkey_send)
		 	{
		 	 	free_ct=3;
			 	free_send =1;
			 	status_drag =0;
			 	fkey_send =0;
		 	}
		 	#endif
		}
	} 
	#endif
	/*
	if(gbCTAP_TouchPalmFlag)
	{
		 fpalm_set =1;
		 gbCTAP_TouchPalmFlag =0;
		 palm_ct =0;
	}
	*/
	/*
	if(fpalm_set)
	{
	 	 if(++palm_ct>=50)
		 	 fpalm_set =0;
	}
	*/
	 if (first_ptp_flag)	//如果是PTP模式并且发送第一次数据
	 {
		mouse_timer_couter += 0x14;			//时间计数器累加2MS
		mouse_timer_couter &= 0xffff;		//保存16位的数据就OK
	 }
	 else
	 {
		mouse_timer_couter = 0;
	 }


	if(delay_allow_read_flag == 0)
	{
		delay_allow_read_cout++;
		if(delay_allow_read_cout >= 1500)
		{
			//delay_allow_read_flag = 1;
			delay_allow_read_cout = 0;
			init_hid_des_flag = 1;
			mouse_mode_flag = 1;
			touch_up_cout = 0;
			no_send_mouse_data_flag = 0;
			mouse_send = 0;
		}
		//delay_allow_read_flag = 0;
	}
}


////;******************************************************************************
////;* Function: CTAP_Coord2OutCoord
////;* Desc.:  
////;* Param: 
////;*			input : geCTMOS_CoordTempBuf[].
////;* Return:
////;*			output : None.
////;* Used Register:
////;*		 	  
////;* Stack:
////;*	Used =  bytes.
////;*	Temp =  bytes.
////;* Notes:
////;*	Main Loop
////;* Cycles:
////;*	
////;******************************************************************************
//void CTI2C_C_StCoord2OutCoord(void)
//{
//	SFR_IE	&= ~BIT7;//Disable All INT
//
//	//St Coord Transfer To Replace Coord
//	CTI2C_C_MemCopy(&__geCTI2C_OutCoord,&geCTMOS_CoordTempBuf,sizeof(__geCTI2C_OutCoord));
//
//	SFR_IE |= BIT7;//Enable All INT
//}


//***********************************************************************************************************************
//* Function: 获取坐标值及判断接触和提笔
//* Desc.: convert coord to OS buffer ( __geCTI2C_OutCoord )
//*			
//* Param:
//*		unsigned char MaxNumTouchs (input)
//*			max number of touchs
//*		unsigned char MaxNumKeys (input)
//*			max number of keys
//*		const unsigned char *pTouchState (input)
//*			pointer to pTouchState flag buffer
//*			0x00 = NO_TOUCH
//*			0x02 = TOUCH
//*			0x20 = TOUCH_UP (first NO_TOUCH)	
//*		const short *PosX (input)
//*			pointer to x position buffer
//*		const short *PosY (input)
//*			pointer to y position buffer
//***********************************************************************************************************************
void CTI2C_C_UpdateOSCoordBuff(unsigned char MaxNumTouchs, unsigned char MaxNumKeys,const  unsigned char near *pTouchState,
							const  short near *PosX,const  short near *PosY)
{
	SFR_IE	&= ~BIT7;//Disable All INT
	if(touch_up_cout != 0)
	{
		gbtCTMOS_I2cCoordBufFullFlag = 1;
		SFR_IE |= BIT7;//Enable All INT
		return;
	}
	//Convert To ST Protocol,  
	//#if(REPLACE_I2C_PROTOCOL == REPLACE_I2C_SITRONIX_A)	 //output OS buffer = __geCTI2C_OutCoord
	if((!mouse_send) && init_hid_des_flag && (!no_send_mouse_data_flag))//&& (!need_send_finger_mun ) 
	{
		CTI2C_C_COORD_XY *pDataXY;
		unsigned char numTouch;//;,i;//,key,b;
		unsigned char tempi,up_touch_num;//;,i;//,key,b;
		short x,y;

		tempi = 0;
		numTouch = 0;
		up_touch_num = 0;
		touch_up_cout = 0;
		touch_have_data = 0;
		
		pDataXY	= &__geCTI2C_OutCoord.XyCoord0;
		
	 	do{
			if(((*pTouchState) & 0x0F) == 0)
			{//no touch
				pDataXY->XyHCoord &= 0x7F;	//set validate bit to 0
				if(old_touch_have_data &(0x01<<tempi))
				{
					touch_up_cout |= (0x01 << tempi);
					gbtCTMOS_I2cCoordBufFullFlag = 1;
					up_touch_num++;
				}
			}
			else
			{//touch
				x = *PosX;
				y = *PosY;	
				pDataXY->XyHCoord = 0x80 | ((x&0xF00)>>4) | ((y&0xF00)>>8);
				pDataXY->XLCoord = x;
				pDataXY->YLCoord = y;
				touch_have_data |= (0x01 << tempi);
				numTouch++;
			}			
			tempi++;
			pTouchState++;	
			pDataXY++;
			PosX++;
			PosY++;
			MaxNumTouchs--;
		}while(MaxNumTouchs != 0);
	
		__geCTI2C_OutCoord.Fingers =  numTouch+up_touch_num;
		old_touch_have_data = touch_have_data;
	   	__geCTI2C_OutCoord.Keys =  MaxNumKeys;
		if(gbCTAP_TouchPalmFlag == 1)
		{
			fpalm_set = 1;
			gbCTAP_TouchPalmFlag = 0;
			fpalm_set_up = 0;
			//fpalm_set_cout = 0;
		}
	}
	//#endif

	SFR_IE |= BIT7;//Enable All INT
}


//-------------------------------------------------------//
//自己处理部分的程序
//-------------------------------------------------------//

//-------------------------------------------------------
//Func	:收取坐标后进行坐标处理
//-------------------------------------------------------
void mtcp_coord(void)
{
	UINT8 tmpi;
  	gbtCTMOS_I2cCoordBufFullFlag =0;
  	
	if(mouse_mode_flag == 1)
  	{
		need_coord_flag = 0;
		finger_num_buf = __geCTI2C_OutCoord.Fingers;
		mouse_timer_buf = mouse_timer_couter;
		need_coord_flag = touch_have_data | touch_up_cout;
		for(tmpi=0;tmpi<5;tmpi++)	//循环
		{
			if((touch_up_cout & (0x01<< tmpi)) !=0)
			{
				touch_up_cout &= ~(0x01<< tmpi);
				finger_cout_data[tmpi] = tmpi;				//手指编号
				finger_cout_status[tmpi] = 0;				//是否有手指
				//need_coord_flag |= (0x01<<tmpi);
				if(fpalm_set == 1)						//在大面积下发现提笔状态
				{
					fpalm_set = 0;						//将大面积设置为0
					fpalm_set_up = 1;					//发送离开大面积
				}
			}
			else if((touch_have_data & (0x01<< tmpi)) != 0)	//判断是第几个
			{
				touch_have_data &= ~(0x01<< tmpi);	//清掉，以免下次判断重复判断
				finger_cout_data[tmpi] = tmpi;				
				finger_cout_status[tmpi] = 0x02;			//有手指
				//need_coord_flag |= (0x01<<tmpi);
				if(fpalm_set == 0)
				{
					fpalm_set_up = 0;
				}
				first_ptp_flag = 1;
			}
			else								//如果是没有数据的则全部为0
			{
				finger_cout_data[tmpi] = 0;
				finger_cout_status[tmpi] = 0;
			}
		}
		mouse_send = 1;
	}
	#if 0
  	if(!__geCTI2C_OutCoord.Fingers)
	{
	 	if(ftouch_sta && pre_point)
		{
		   	mv_datax =0;
		   	mv_datay =0;
		   	if(status_drag ||status_key)
			{
			 	if(status_drag) send_need =0;
			 	move_volid_count =0;
			 	touch_rece_free(); 			  //清状态
			 	if(status_key_full)
					key_reg =0;
			 	status_key_full =0;
			 	free_ct=3;
			 	free_send =1;
			}
		   	touch_data_clrr();
		}
	}
  	else 
	{
   		ftouch_sta =1;
   		hcoord[0] =__geCTI2C_OutCoord.XyCoord0.XyHCoord;
   		hcoord[1] =__geCTI2C_OutCoord.XyCoord1.XyHCoord;
   		touch_order();
   		if(!status_drag && status_move)
   		{
			if(++move_volid_count >10)	//iic因为要等待至少6ms才能发送数据，所以设10次
	   			status_drag =1;
   		}
   		if(status_drag)
   			fkey_hit_i =1;
   		if(key_line_M)
   			fkey_hit_i =0;
   		if(__geCTI2C_OutCoord.Fingers ^pre_point)
   		{
			pre_point =__geCTI2C_OutCoord.Fingers;
			move_volid_count=0;
			touch_data_clrr();
			fgest_out =1;
			free_ct =3;
			if(fkey_hit_i)
	 		{
	 	 		if(pre_point>1)
	 	 		{
	 	 	 		status_key_full =0;//加手指能点击
			 		status_drag =0;//加手指能点击
	 	 		}
		 		else if(pre_point == 0)
				{
		 	   		if(!status_drag)
		         			free_send =1;//发送按键断码三次
		 	   		else 
					{
			   	 		send_need =0;
				 		key_reg =0;
			   		}
		 		}
	 		}
   		}
   		if(!key_line_M)
   		{
   	 		mid_key_L =0;//这里清除这几个标志是因为如果两个手指按下左右键，然后离开一个手指再松开按键会出现不进入后边的判断程序，执行后边的return了
     			mid_key_R =0;
	 		mid_valid =0;
	  		if(__geCTI2C_OutCoord.Fingers>point_max) 
		  		point_max=__geCTI2C_OutCoord.Fingers;  
   		}

   		touch_mid_move();
   		if((hcoord[1]&0x80)&&(pre_point==1))
 		{
 	 		point_data[0] =point_data[2];
 	 		point_data[1] =point_data[3];
 		}

   		//touch_move_multi();
   
   		status_key =0; 
   		mouse_send =1;
   		if(!status_drag)
		{
   	 		status_key =1;
	 		if(fkey_hit_i && (pre_point==1))
				status_key =0;
   		}
  	}
	#endif
}
//-------------------------------------------------------
//Func	:计算相对坐标
//Desc	: 
//Input :	
//Output:
//Return: 
//Author: Owen	
//Date	: 2014/07/11
//-------------------------------------------------------
#if 0
void touch_order(void)
{
 	UINT8 B[2],C[2],i;
 	UINT16 dat;

 	B[0] =__geCTI2C_OutCoord.XyCoord0.XLCoord;
 	B[1] =__geCTI2C_OutCoord.XyCoord1.XLCoord;
 	C[0] =__geCTI2C_OutCoord.XyCoord0.YLCoord;
 	C[1] =__geCTI2C_OutCoord.XyCoord1.YLCoord;
 	status_move =0;
 	for(i=0;i<2;i++)
 	{
	 	dat  =B[i]<<4;
	 	dat  |=(hcoord[i]&0x70)<<8;
	 	dat <<=1;
	 	point_data[i*2] =(touch_order_over(dat-pre_datax[i]))&0x00ff;
	 	pre_datax[i]=dat;
 	 	dat  =C[i];
	 	dat  |=(hcoord[i]&0x07)<<8;
	 	dat <<=5;
	 	point_data[1+i*2] =(touch_order_over(dat-pre_datay[i]))&0x00ff;
	 	pre_datay[i] =dat;
	 	if(point_data[i*2])  status_move =1;
     		if(point_data[1+i*2])  status_move =1;
 	}
  	if(!mv_datax && !mv_datay)
 	{
 	 	mv_datax =pre_datax[0];
	 	mv_datay =pre_datay[0];
 	}
  	if(!status_drag)
 	{
 	 	if(pre_datax[0] || pre_datay[0])
 	 	{
 	 	 	if(abs(pre_datax[0]-mv_datax)>(70<<5))//位移超过两个轴
 	 	   		status_drag =1;
 	 	 	if(abs(pre_datay[0]-mv_datay)>(70<<5))//位移超过两个轴
 	 	   		status_drag =1;
 	 	}
 	}
}

//-------------------------------------------------------
//Func	:改超出0x7f数据出错
//Desc	: 
//Input :	
//Output:
//Return: 
//Author: Owen	
//Date	: 2015/03/18
//-------------------------------------------------------
INT16 touch_order_over(INT16 buf)
{
  	if(abs(buf)>(0x7f<<5))
	{
		if(buf&0x8000)
	 	{
	 	 	 buf =(0x80<<5);
	 	}
		 else buf =(0x7f<<5);
	}
  	return buf>>5;
}

#define X0 my_abs(point_data[0])
#define X1 my_abs(point_data[2])
#define Y0 my_abs(point_data[1])
#define Y1 my_abs(point_data[3])
#endif
//-------------------------------------------------------
//Func	: my_abs
//Desc	: 8位数取绝对值
//Input :   
//Output: 
//Return: 
//Author: Owen	
//Date	: 2013/06/13
//-------------------------------------------------------
#if 0
INT8 my_abs(UINT8 buf)
{
 	if(buf &0x80)
 	{
 	 	buf = ~buf;
	 	buf++;
 	}
 	return buf;
}
#endif
//-------------------------------------------------------
//Func	: touch_mid_move_sub
//Desc	: 中键位移判定
//Input :   
//Output:     
//Return: 
//Author: Owen	
//Date	: 2013/06/13
//-------------------------------------------------------
#if 0
void touch_mid_move(void)
{ 
 	static uint16 edata mid_x,mid_y;
 	uint8 i;
 	uint16 A[2];
 	bit flag;

 	if(key_line_M)
  	{
		flag =0;
		MID_LR_STR:
	  	//判断左右键
		if(!mid_valid)
		{
	  		mid_y =0;
	  		for(i=0;i<2;i++)
			{
		 		if(hcoord[i]&0x80)
				{ 
			 		if(pre_datay[i]<touch.resy_mid)
					{
				 		if(flag)
							continue;
				 		else 
							mid_key_L=1;//右下右键其余左键
					}
			 		else if(pre_datax[i]>(gtCTMOS_I2cResX<<4)) //右下右键其余左键
			 			//if(pre_datax[i]>(gtCTMOS_I2cResX<<4))//左边左键 右边右键
						mid_key_R =1;
			 		else 
						mid_key_L=1;
			 		if(!flag)
					{
				 		if(mid_key_L && mid_key_R)
						{
							flag =1;
					 		mid_key_L=0;
					 		mid_key_R=0;
					 		goto MID_LR_STR;
						}
					}
			 
			 		if(mid_y<pre_datay[i])//150709加入中微天先按一个手指再按按键的情况，先手指也要可以移动
					{
				 		mid_x=pre_datax[i];
				 		mid_y=pre_datay[i];
					}
				}
			}
	  		mid_valid =1;
		}
		if((mid_valid)&&(pre_point>1))
		{
	 		for(i=0;i<2;i++)
	 		{
	  			if(hcoord[i]&0x80)
		 			A[i] =abs(pre_datax[i]-mid_x)+abs(pre_datay[i]-mid_y);
	  			else A[i] =0;
	 		}
	 		if(A[1]>A[0])
			{
		 		point_data[0] = point_data[2];
		 		point_data[1] = point_data[3];
			}
	 		status_drag =1;
	 		point_data[3] =0;
		}
		else 
		{
		 	point_data[0] =0;
		 	point_data[1] =0;
		}
  	}
}
#endif
//-------------------------------------------------------
//Func  : TOUCH_TRAN
//Desc	: DATA发送
//Input	:
//Output: 
//Return: 
//Author: Owen  
//Date	: 2013/05/9
//-------------------------------------------------------
void TOUCH_TRAN(void)
{
  	UINT8  dat,dev,i;//,j;
  	static UINT8 TRAN_CT=0;
  	UINT8  tempi=0;
	CTI2C_C_COORD_XY *bDataXY;
  	mouse_send =0;
  	fiic_inv =0;
   	dat=0;
	
   	#if 0
   	if(fgest_out)
	{
	   	point_data[0] =0;
	   	point_data[1] =0;
		
	   	if(!free_send)
		  	free_ct--;
	   	if(!free_ct)
		  	fgest_out =0;
	}
	#endif
 	if(key_line_M) 
   	{
   		dat = 0x01;
   		#if 0
    		dat |=0x8;
		
		if(mid_key_L)		dat |=0x01;
		if(mid_key_R) 		dat |=0x02;
		send_need =0;
		#endif
   	} 
	#if 0
 	if(free_send)
  	{
   		if(!(--free_ct))
     		{
      			free_send =0;
	  		fgest_out =0;
      			if(!key_line_M)
	 		{
	 	 		mid_key_L =0;
		 		mid_key_R =0;
		 		mid_valid=0;
	 		}
     		}
  	}
 	else if(send_need)	 
 	 	dat |=key_reg;    //这部分后续可以留着省空间  只会有一种模式
	
   	dat |=point_data[3]<<4;
   	if(TRAN_CT&0x1)
   		dat |=0x40;
	
   	if(fpalm_set)
   		dat |=0x80;
   	#endif
   	if(!fi2c_str)
		SFR_IE	&= ~BIT7;//Disable All INT
    	
   	dev =IN_DATA_LENG;
   	if(buf_mem2)//buf2满
   	{
   	 	for(i=0;i<IN_DATA_LENG;i++)
	 		iic_buf[0+i] =iic_buf[IN_DATA_LENG+i];
   	}
   	else if(!buf_mem1)//buf1空 buf2空
   		dev =0;
	
	if(!dev)
   		buf_mem1 =1;
   	else 
		buf_mem2 =1;
	/*
   	iic_buf[1+dev]= dat;
   	if(!fpalm_set)
   	{
     		iic_buf[2+dev]= point_data[0];
     		iic_buf[3+dev]= point_data[1];
   	}
   	else iic_buf[1+dev] &= ~0x7;
   
	   
   	if(!dev)
   		buf_mem1 =1;
   	else 
		buf_mem2 =1;
   
   	iic_buf[4+dev]= __geCTI2C_OutCoord.XyCoord0.XyHCoord;  
   	iic_buf[5+dev]= __geCTI2C_OutCoord.XyCoord0.XLCoord; 
   	iic_buf[6+dev]= __geCTI2C_OutCoord.XyCoord0.YLCoord;

   	iic_buf[7+dev]= __geCTI2C_OutCoord.XyCoord1.XyHCoord;  
   	iic_buf[8+dev]= __geCTI2C_OutCoord.XyCoord1.XLCoord; 
   	iic_buf[9+dev]= __geCTI2C_OutCoord.XyCoord1.YLCoord;

   	iic_buf[10+dev]= __geCTI2C_OutCoord.XyCoord2.XyHCoord;  
   	iic_buf[11+dev]= __geCTI2C_OutCoord.XyCoord2.XLCoord; 
   	iic_buf[12+dev]= __geCTI2C_OutCoord.XyCoord2.YLCoord;

   	iic_buf[13+dev]= __geCTI2C_OutCoord.XyCoord3.XyHCoord;  
   	iic_buf[14+dev]= __geCTI2C_OutCoord.XyCoord3.XLCoord; 
   	iic_buf[15+dev]= __geCTI2C_OutCoord.XyCoord3.YLCoord;

   
   	iic_buf[0+dev]= 0x54;
	*/
	if(mouse_mode_flag == 1)
	{
		iic_buf[0+dev] = 0x20;			//装载数据长度低8位	LENTH_L
		iic_buf[1+dev] = 0x00;			//数据长度高8位	LENTH_H
		iic_buf[2+dev] = MUTI_TOUCHPAD_REPORT;	//数据的ID	RPORT_ID
		bDataXY	= &__geCTI2C_OutCoord.XyCoord0;
		
		for(i=0;i<5;i++)	//检测有几个有效值
		{
			//tempi = 5*i;
			if(need_coord_flag & (0x01<<i))	//如果第一个有变化
			{
				tempi = vail_touch_count*5;
				iic_buf[tempi+3+dev] = finger_cout_status[i];
				if(!fpalm_set)				//如果是大面积			    
		   		{
		   			iic_buf[tempi+3+dev] |= 0x01;	//设置对应位
		   		}
				if(fpalm_set_up == 1)
				{
					iic_buf[tempi+3+dev] = 0;	//设置对应位
				}
				iic_buf[tempi+3+dev] |= (finger_cout_data[i] << 2);		// FINGER_ID
				//iic_buf[tempi+4+dev] = finger_cout_data[i];
				iic_buf[tempi+4+dev] = (bDataXY+i)->XLCoord;//__geCTI2C_OutCoord.XyCoord0.XLCoord; 		//
				iic_buf[tempi+5+dev] = ((bDataXY+i)->XyHCoord >> 4) & 0x07;//(__geCTI2C_OutCoord.XyCoord0.XyHCoord >> 4) & 0x07;
				iic_buf[tempi+6+dev] = (bDataXY+i)->YLCoord;//__geCTI2C_OutCoord.XyCoord0.YLCoord;
				iic_buf[tempi+7+dev] = (bDataXY+i)->XyHCoord & 0x07;//__geCTI2C_OutCoord.XyCoord0.XyHCoord & 0x07;
				vail_touch_count ++;
			}
			/*
			else
			{
				for(j=0;j<5;j++)
				{
					iic_buf[tempi+3+dev+j] = 0;
				}
			}
			*/
		}
		need_coord_flag = 0;
		iic_buf[28+dev] = (UINT8)(mouse_timer_buf & 0xff);	//时间低8位
		iic_buf[29+dev] = (UINT8)((mouse_timer_buf>>8) & 0xff);	//时间高8位
		iic_buf[30+dev] = finger_num_buf;	//设置为手指个数    FINGER_NUM
		iic_buf[31+dev] = dat;		//BUTTON
		vail_touch_count = 0;
	}
	finger_num_buf = 0;
   	fint_low =1;
   	INT_O =0;
   	//P11 =0;
   	SFR_IE |= BIT7;//Enable All INT
   	TRAN_CT++;
}
//-------------------------------------------------------
//Desc	: 按键扫描
//-------------------------------------------------------
void key_loop(void)
{
	BYTE a,buff;		

	
	if(fint_low)
		return;

	fkey_inv=0;
	
	SFR_P1DIR = 0x05;	//输入
	
	//if(key_check) key_check_str=1;	//上次按键有变化，设置为1
	//key_check =0;
	
	buff = 0;
	
	//if(!P11 || !P10) buff |=0x1;
	if(!P12)  
	{
		buff = 0x01;
	}
	SFR_P1DIR &= ~0x02;  //输出
	
	a =buff^key_bool_bit;				//比较上次状态
	a &=0x1;							//保留最后一位
	if(!a) 							//如果是0，表示与上次一致，没有状态变化
	{
		if(key_line_M == 0)	//而且上次的值也为0
			return;
	}
	/*
	else  
		key_check =1;				//如果是有变化
	if(!key_check_str)				//
		return;
	*/
	key_bool_bit &=~0x7;
	key_bool_bit |=buff;
	if(old_touch_have_data == 0)
	{
		mouse_send =1;
		mouse_timer_buf = mouse_timer_couter;	
		first_ptp_flag = 1;
	}
	#if 0
	if(key_line_M)
		status_drag =1;
	fgest_out =1;
	free_ct =20;
	#endif
}
//-------------------------------------------------------
//Desc	: 计时程序
//-------------------------------------------------------
void int_count_check(void)
{
 	static BYTE edata int_200ms_key_count,int_key_out_count;
	static UINT16	init_enable_count=0;
   	int_flag=0;
	/*
   	if(fpalm_set)
	{
		#if 0
	 	status_drag =1;
		#endif
	 	mouse_send =1;
	}
	*/
	#if 0
	if(init_hid_des_flag == 0)
	{
		init_enable_count++;
		if(init_enable_count >= 500)		//5S
		{
			init_enable_count = 0;
			init_hid_des_flag = 1;
		}
	}
	#endif
	#if 0
   	if(!status_drag)
	{
	 	if(free_send ||send_need)
	  	{
	   		mouse_send =1;
	   		touch_data_clrr();
	  	}
	  
	 	if(status_key)
		{
		 	int_key_out_count =0;
		 	if(++int_200ms_key_count>=25)
			{
			 	status_drag =1;
			 	status_key_full =1;
			}
		}
	 	else 
		{
		 	if(!status_key_full && (int_200ms_key_count>1))
		   		send_need =1;
		 	if(fkey_hit_i) key_reg =1;
		 	if(send_need)
		  	{
		   		if(++int_key_out_count>15)
				{
			 		int_key_out_count =0;
			 		send_need =0;
			 		key_reg =0;
			 		free_ct=5;
			 		free_send =1;
			 		mouse_send=1;
			 		if(fkey_hit_i)
						status_drag =1;//复位status_drag
			 		touch_data_clrr();
				}
		  	}
		 	else int_key_out_count =0;
		 	int_200ms_key_count =0;
		}
	}
   	else
	{
		int_200ms_key_count =0;
		int_key_out_count =0;
		status_key_full =0;
		status_key =0;
		if(key_reg>1)
			key_reg =0;//左键需要拖动功能，其他不需要
	}
	#endif
}



//-------------------------------------------------------
//Desc	: touch数据清BUFF
//-------------------------------------------------------
void touch_rece_free(void)
{
	 touch_data_clrr();
	 #if 0
	 if(point_max ==3)
	 	 key_reg =0x04;
	 else if(point_max <3)
	 	key_reg =point_max;
	 else key_reg =0;
	 #endif
	 
	 pre_point =0;
	 point_max =0;
	 first_flag =0;
	 tp_bool_bit =0;
}
//-------------------------------------------------------
//Desc	: touch数据清BUFF
//-------------------------------------------------------
void touch_data_clrr(void)
{
	point_data[0] =0;
	point_data[1] =0;
	point_data[2] =0;
	point_data[3] =0;
}
//-------------------------------------------------------
//Desc	: 相对坐标滤波
//-------------------------------------------------------
#if 0
void touch_filter_sub(void)
{
	static INT16 edata pre_pointx_data=0,pre_pointy_data=0;
 
   /*if(!iic_buf[2] && !iic_buf[3])
	 {
	  pre_pointx_data =0;
	  pre_pointy_data =0;
	  return;
	 }*/
	first_order_filter(&iic_buf[2],&pre_pointx_data);
	first_order_filter(&iic_buf[3],&pre_pointy_data);
}

 void first_order_filter(INT8	*now, INT16  *old)//old为放大3位值
{//2/8  +6/8
   union cyp
   {
	INT16 INT_data;
	INT8 CHAR_data[2];
   };
   union cyp val;
   
   val.CHAR_data[1] =0;
   val.CHAR_data[0] =*now;
   
#if  TRACE_SPEED==SPEED_1
   val.INT_data >>=5; //放大三位
   *old <<=1;
   val.INT_data =  (val.INT_data<<2)+(val.INT_data<<1);
   *old = (*old + val.INT_data)>>3; // /8
   *now = ((*old+0x0004)>>3)&0x00ff; 
#elif   TRACE_SPEED==SPEED_2
	val.INT_data >>=3; //放大三位	 再 *4
	*old =	(*old<<2);
	*old = (*old + val.INT_data)>>3; // /8
	*now = ((*old+0x0004)>>3)&0x00ff;
#elif	TRACE_SPEED==SPEED_3
   val.INT_data >>=4; //放大三位  再 *2
   *old =  (*old<<2)+(*old<<1);
   *old = (*old + val.INT_data)>>3; // /8
   *now = ((*old+0x0004)>>3)&0x00ff;
#endif
}
#endif
//#endif//(CTMOS_I2C_REPL_C_IF==1)

