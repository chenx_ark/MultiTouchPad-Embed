;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTISP_MAIN_H__
#define	__CTISP_MAIN_H__

EXTERN	CODE	(CTISP_Init)
EXTERN	CODE	(CTISP_JmpIspHandler)

#endif	//__CTISP_MAIN_H__