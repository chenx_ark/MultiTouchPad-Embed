;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTCAL_MAIN_H__
#define	__CTCAL_MAIN_H__

#define		CTCAL_ClrEram					CTCAL_FixedFunc_ClrEram
#define		CTCAL_FillEram					CTCAL_FixedFunc_FillEram
#define		CTCAL_MoveEramToWordEram		CTCAL_FixedFunc_MoveEramToWordEram
#if(CTMOS_ISP_TOUCH_FW2_EN==0)
#define		CTCAL_MoveRomToEram				CTCAL_FixedFunc_MoveRomToEram
#define		CTCAL_MoveEramToEram			CTCAL_FixedFunc_MoveEramToEram
#define		CTCAL_MoveWordEramToWordEram	CTCAL_FixedFunc_MoveWordEramToWordEram
#endif//(CTMOS_ISP_TOUCH_FW2_EN==0)

EXTERN	CODE	(CTCAL_ClrEram) 
EXTERN	CODE	(CTCAL_FillEram) 
EXTERN	CODE	(CTCAL_MoveEramToWordEram)
EXTERN	CODE	(CTCAL_MoveRomToEram)
EXTERN	CODE	(CTCAL_MoveEramToEram)
EXTERN	CODE	(CTCAL_MoveWordEramToWordEram)

EXTERN	CODE	(CTCAL_ClrWordEram)
EXTERN	CODE	(CTCAL_FillWordEram)
//EXTERN	CODE	(CTCAL_MoveFwTabToEram)
EXTERN	CODE	(CTCAL_UnsigndAddSignedSaturationProtect)
//EXTERN	CODE	(CTCAL_ChecksumCalculation)
//EXTERN	CODE	(CTCAL_RomRamChecksumCalculation)
EXTERN	CODE	(CTCAL_ChecksumCalculation1)
EXTERN	CODE	(CTCAL_ChecksumCalculation2)

#endif	//__CTCAL_MAIN_H__