;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTSYS_MAIN_H__
#define	__CTSYS_MAIN_H__

EXTERN	CODE	(CTSYS_SysEnvInit)
//EXTERN	CODE	(CTSYS_Timerx_Ini)
//EXTERN	CODE	(CTSYS_CLK_TRIM)

#endif	//__CTSYS_MAIN_H__