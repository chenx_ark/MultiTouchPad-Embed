;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTALG_MAIN_H__
#define	__CTALG_MAIN_H__

EXTERN	CODE	(CTALG_CoordTransform)
EXTERN	CODE	(CTALG_Init)
EXTERN	CODE	(CTALG_Main)
//20130926 kchsu//EXTERN	CODE	(CTALG_GetNoiseInfo)





#endif	//__CTALG_MAIN_H__