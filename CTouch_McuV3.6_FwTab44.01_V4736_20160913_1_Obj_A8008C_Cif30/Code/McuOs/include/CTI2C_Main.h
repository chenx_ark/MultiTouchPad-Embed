;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTI2C_MAIN_H__
#define	__CTI2C_MAIN_H__

EXTERN	CODE	(CTI2C_Init)
EXTERN	CODE	(CTI2C_IntSr)
EXTERN	CODE	(CTI2C_I2cProcessing)

#endif	//__CTI2C_MAIN_H__