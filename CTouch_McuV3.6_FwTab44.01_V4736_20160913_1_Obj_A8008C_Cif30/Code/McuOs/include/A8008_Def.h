;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef __A8008_DEF_H__
#define __A8008_DEF_H__


#define		BIT7b	7
#define		BIT6b	6
#define		BIT5b	5
#define		BIT4b	4
#define		BIT3b	3
#define		BIT2b	2
#define		BIT1b	1
#define		BIT0b	0

#define		BIT15	0x8000
#define		BIT14	0x4000
#define		BIT13	0x2000
#define		BIT12	0x1000
#define		BIT11	0x0800
#define		BIT10	0x0400
#define		BIT9	0x0200
#define		BIT8	0x0100

#define		BIT7	0x80
#define		BIT6	0x40
#define		BIT5	0x20
#define		BIT4	0x10
#define		BIT3	0x08
#define		BIT2	0x04
#define		BIT1	0x02
#define		BIT0	0x01

#define		UPPER32_1			0xFFFF0000
#define		UPPER32_0			0x0000FFFF

sfr ACC				= 0xE0;
sfr B				= 0xF0;
;==[0x80]============================================================
sfr SFR_P0 			= 0x80;
		SFR_P07_BIT				BIT		SFR_P0.7;
		SFR_P06_BIT				BIT		SFR_P0.6;
		SFR_P05_BIT				BIT		SFR_P0.5;
		SFR_P04_BIT				BIT		SFR_P0.4;
		SFR_P03_BIT				BIT		SFR_P0.3;
		SFR_PACKAGE_SEL			BIT		SFR_P0.3;
		SFR_P02_BIT				BIT		SFR_P0.2;
		//SFR_P01_BIT				BIT		SFR_P0.1;
		//SFR_P00_BIT				BIT		SFR_P0.0;
sfr SFR_SP 			= 0x81;
sfr SFR_DPL			= 0x82;
	sfr DPL			= 0x82;
sfr SFR_DPH			= 0x83;
	sfr DPH			= 0x83;
sfr SFR_DPXL		= 0x84;
//sfr SFR_85			= 0x85;
//sfr SFR_86			= 0x86;
sfr SFR_PCON		= 0x87;
		//SFR_PCON_PD_BIT			BIT		SFR_PCON^1;
		//SFR_PCON_IDL_BIT		BIT		SFR_PCON^0;
//sfr SFR_88			= 0x88;
sfr SFR_CLKO		= 0x89;
//	#define	CLKOS_CLR			(0xFF-(7<<2))
//	#define	CLKOS_OSC			(0<<2)
//	#define	CLKOS_20K			(1<<2)
//	#define	CLKOS_48M			(2<<2)
//	#define	CLKOS_XTL			(3<<2)
//	#define	CLKOS_PLL			(4<<2)
//	#define	CLKOS_T0OUT			(5<<2)
//	#define	CLKOD_CLR			(0xFF-(3<<0))
//	#define	CLKOD_DIV1			(0<<0)
//	#define	CLKOD_DIV2			(1<<0)
//	#define	CLKOD_DIV4			(2<<0)
//	#define	CLKOD_DIV8			(3<<0)
sfr SFR_T0L			= 0x8A;
sfr SFR_T0H			= 0x8B;
		//SFR_T0H_T0EN_BIT		BIT SFR_T0H^7;
sfr SFR_T1L			= 0x8C;
sfr SFR_T1H			= 0x8D;
		//SFR_T1H_T1EN_BIT		BIT SFR_T1H^7;
sfr SFR_PRS			= 0x8E;
		//SFR_PRS_SENA_BIT		BIT SFR_PRS^6;	
//sfr SFR_8F		= 0x8F;
;==[0x90]============================================================
sfr SFR_P1 			= 0x90;
		//SFR_CTP_INT_BIT			BIT		SFR_P1.1;
		SFR_P13_BIT				BIT		SFR_P1.3;
		SFR_P12_BIT				BIT		SFR_P1.2;
		SFR_P11_BIT				BIT		SFR_P1.1;
		SFR_P10_BIT				BIT		SFR_P1.0;
sfr SFR_SCTR		= 0x91;
//		SFR_SCTR_SPIEN_BIT		BIT SFR_SCTR^7;
//		SFR_SCTR_RXIEN_BIT		BIT SFR_SCTR^6;
//		SFR_SCTR_ERIEN_BIT		BIT SFR_SCTR^5;
//		SFR_SCTR_MEREN_BIT		BIT SFR_SCTR^4;
//		SFR_SCTR_POL_BIT		BIT SFR_SCTR^2;
//		SFR_SCTR_PHA_BIT		BIT SFR_SCTR^1;
//		SFR_SCTR_SMOD_BIT		BIT SFR_SCTR^0;
sfr SFR_SCKR		= 0x92;
sfr SFR_SSR			= 0x93;
//		SFR_SSR_RXRDY_BIT		BIT SFR_SSR^6;
//		SFR_SSR_TXEMP_BIT		BIT SFR_SSR^5;
//		SFR_SSR_SBZ_BIT			BIT SFR_SSR^4;
//		SFR_SSR_MDERR_BIT		BIT SFR_SSR^2;
//		SFR_SSR_OERR_BIT		BIT SFR_SSR^1;
//		SFR_SSR_BERR_BIT		BIT SFR_SSR^0;
sfr SFR_SDATAL		= 0x94;
sfr SFR_SDATAH		= 0x95;
//sfr SFR_96			= 0x96;
//sfr SFR_97			= 0x97;
//sfr SFR_98			= 0x98;
sfr SFR_LVR			= 0x99;
sfr SFR_CLR10K		= 0x9A;
sfr SFR_CNT10K		= 0x9B;
sfr SFR_LDOPD		= 0x9C;
		//SFR_LDOWK_WKV33H_BIT	BIT SFR_LDOWK^4;
		//SFR_LDOWK_WKV33L_BIT	BIT SFR_LDOWK^3;
		//SFR_LDOWK_WK50M_BIT		BIT SFR_LDOWK^1;
		//SFR_LDOWK_WK50U_BIT		BIT SFR_LDOWK^0;
sfr SFR_LDOFT		= 0x9D;
		//SFR_LDOPD_PDV33H_BIT	BIT SFR_LDOPD^4;
		//SFR_LDOPD_PDV33L_BIT	BIT SFR_LDOPD^3;
		//SFR_LDOPD_PDFL_BIT		BIT SFR_LDOPD^2;
		//SFR_LDOPD_PD100M_BIT	BIT SFR_LDOPD^1;
		//SFR_LDOPD_PD50U_BIT		BIT SFR_LDOPD^0;
sfr SFR_V18			= 0x9E;
sfr SFR_V25			= 0x9F;
;==[0xA0]============================================================
//sfr SFR_A0			= 0xA0;
sfr SFR_MPAGE		= 0xA1;
sfr SFR_P0DIR		= 0xA2;
		SFR_P0DIR_7_BIT		BIT 	SFR_P0DIR.7;
		SFR_P0DIR_6_BIT		BIT 	SFR_P0DIR.6;
		SFR_P0DIR_5_BIT		BIT 	SFR_P0DIR.5;
		SFR_P0DIR_4_BIT		BIT 	SFR_P0DIR.4;
		SFR_P0DIR_3_BIT		BIT 	SFR_P0DIR.3;
		SFR_P0DIR_2_BIT		BIT 	SFR_P0DIR.2;
		//SFR_P0DIR_1_BIT		BIT 	SFR_P0DIR.1;
		//SFR_P0DIR_0_BIT		BIT 	SFR_P0DIR.0;
sfr SFR_P1DIR		= 0xA3;
		SFR_P1DIR_3_BIT		BIT 	SFR_P1DIR.3;
		SFR_P1DIR_2_BIT		BIT 	SFR_P1DIR.2;
		SFR_P1DIR_1_BIT		BIT 	SFR_P1DIR.1;
		SFR_P1DIR_0_BIT		BIT		SFR_P1DIR.0;
sfr SFR_P0PU		= 0xA4;
		SFR_P0PU_7_BIT			BIT		SFR_P0PU.7;
		SFR_P0PU_6_BIT			BIT		SFR_P0PU.6;
		SFR_P0PU_5_BIT			BIT		SFR_P0PU.5;
		SFR_P0PU_4_BIT			BIT		SFR_P0PU.4;
		SFR_P0PU_3_BIT			BIT		SFR_P0PU.3;
		SFR_P0PU_2_BIT			BIT		SFR_P0PU.2;
		//SFR_P0PU_1_BIT			BIT		SFR_P0PU.1;
		//SFR_P0PU_0_BIT			BIT		SFR_P0PU.0;
sfr SFR_P1PU		= 0xA5;
		SFR_P1PU_3_BIT			BIT		SFR_P1PU.3;
		SFR_P1PU_2_BIT			BIT		SFR_P1PU.2;
		SFR_P1PU_1_BIT			BIT		SFR_P1PU.1;
		SFR_P1PU_0_BIT			BIT		SFR_P1PU.0;
sfr SFR_P0FL		= 0xA6;
		//#define	P07FL_GPIO			(0<<7)
		//#define	P07FL_RESV			(1<<7)
		//#define	P07FL_SPI_MISO		(0<<7)
		//#define	P07FL_SPI_MOSI		(1<<7)
		
		//#define	P06FL_GPIO			(0<<6)
		//#define	P06FL_I2C_SDA		(1<<6)
		//#define	P06FL_SPI_MOSI		(0<<6)
		//#define	P06FL_SPI_MISO		(1<<6)
		
		//#define	P05FL_GPIO			(0<<5)
		//#define	P05FL_RESV			(1<<5)
		//#define	P05FL_SPI_SCK		(0<<5)
		//#define	P05FL_RESV			(1<<5)
				   
		//#define	P04FL_GPIO			(0<<4)
		//#define	P04FL_I2C_SCL		(1<<4)
		//#define	P04FL_SPI_SS		(0<<4)
		//#define	P04FL_RESV			(1<<4)
sfr SFR_P0FH		= 0xA7;
		//#define	P07FH_GPIO			(0<<7)
		//#define	P07FH_RESV			(0<<7)
		//#define	P07FH_SPI_MISO		(1<<7)
		//#define	P07FH_SPI_MOSI		(1<<7)
		
		//#define	P06FH_GPIO			(0<<6)
		//#define	P06FH_I2C_SDA		(0<<6)
		//#define	P06FH_SPI_MOSI		(1<<6)
		//#define	P06FH_SPI_MISO		(1<<6)
		
		//#define	P05FH_GPIO			(0<<5)
		//#define	P05FH_RESV			(0<<5)
		//#define	P05FH_SPI_SCK		(1<<5)
		//#define	P05FH_RESV1			(1<<5)
				   
		//#define	P04FH_GPIO			(0<<4)
		//#define	P04FH_I2C_SCL		(0<<4)
		//#define	P04FH_SPI_SS		(1<<4)
		//#define	P04FH_RESV			(1<<4)
sfr SFR_IE 			= 0xA8;
		SFR_IE_EA_BIT			BIT SFR_IE^7;	
		SFR_IE_TMXE_BIT			BIT SFR_IE^6;
		SFR_IE_ADE_BIT			BIT SFR_IE^5;
		SFR_IE_UARTE_BIT		BIT SFR_IE^4;
		SFR_IE_T1E_BIT			BIT SFR_IE^3;
		SFR_IE_T0E_BIT			BIT SFR_IE^1;
sfr SFR_P1FL		= 0xA9;
		//#define	P12FL_GPIO				(0<<2)
		//#define	P12FL_UART_TX			(1<<2)
		//#define	P12FL_SYNC_RESV			(0<<2)
		//#define	P12FL_RESV				(1<<2)

		//#define	P11FL_GPIO				(0<<1)
		//#define	P11FL_UART_RX			(1<<1)
		//#define	P11FL_CLKO				(0<<1)
		//#define	P11FL_RESV				(1<<1)

		//#define	P10FL_GPIO				(0<<0)
		//#define	P10FL_UART_TX			(1<<0)
		//#define	P10FL_SYNC				(0<<0)
		//#define	P10FL_RESV				(1<<0)
sfr SFR_P1FH		= 0xAA;
		//#define	P12FH_GPIO				(0<<2)
		//#define	P12FH_UART_TX			(0<<2)
		//#define	P12FH_SYNC_RESV			(1<<2)
		//#define	P12FH_RESV				(1<<2)

		//#define	P11FH_GPIO				(0<<1)
		//#define	P11FH_UART_RX			(0<<1)
		//#define	P11FH_CLKO				(1<<1)
		//#define	P11FH_RESV				(1<<1)

		//#define	P10FH_GPIO				(0<<0)
		//#define	P10FH_UART_TX			(0<<0)
		//#define	P10FH_SYNC				(1<<0)
		//#define	P10FH_RESV				(1<<0)
//sfr SFR_AB			= 0xAB;
//sfr SFR_AC			= 0xAC;
//sfr SFR_AD			= 0xAD;
//sfr SFR_AE			= 0xAE;
//sfr SFR_AF			= 0xAF;
;==[0xB0]============================================================
//sfr SFR_B0			= 0xB0;
//sfr SFR_B1			= 0xB1;
//sfr SFR_B2			= 0xB2;
//sfr SFR_B3			= 0xB3;
//sfr SFR_B4			= 0xB4;
//sfr SFR_B5			= 0xB5;
//sfr SFR_B6			= 0xB6;
sfr SFR_IPH			= 0xB7;
//		SFR_IPH_TMXP_BIT		BIT SFR_IPH^6;
//		SFR_IPH_DMAP_BIT		BIT SFR_IPH^5;
//		SFR_IPH_UARTP_BIT		BIT SFR_IPH^4;
//		SFR_IPH_T1P_BIT			BIT SFR_IPH^3;
//		SFR_IPH_T0P_BIT			BIT SFR_IPH^1;
sfr SFR_IPL			= 0xB8;
//		SFR_IPL_TMXP_BIT		BIT SFR_IPL^6;
//		SFR_IPL_DMAP_BIT		BIT SFR_IPL^5;
//		SFR_IPL_UARTP_BIT		BIT SFR_IPL^4;
//		SFR_IPL_T1P_BIT			BIT SFR_IPL^3;
//		SFR_IPL_T0P_BIT			BIT SFR_IPL^1;
sfr SFR_MISC		= 0xB9;
		#define		MISC_SFRIND					BIT7
		#define		MISC_ILADM					BIT5
		#define		MISC_RSTFT					BIT4
		#define		MISC_RSTCS					BIT0
sfr SFR_RST			= 0xBA;
//		SFR_RST_WDTRST_BIT		BIT SFR_RST^2;
//		SFR_RST_ILARST_BIT		BIT SFR_RST^1;
//		SFR_RST_TMXRST_BIT		BIT SFR_RST^0;
sfr SFR_WDT			= 0xBB;
		#define		WDT_WDTM_WDTPS_00125MS		(7<<3)|1
		#define		WDT_WDTM_WDTPS_0025MS		(6<<3)|1
		#define		WDT_WDTM_WDTPS_0050MS		(5<<3)|1
		#define		WDT_WDTM_WDTPS_0100MS		(4<<3)|1
		#define		WDT_WDTM_WDTPS_0200MS		(3<<3)|1
		#define		WDT_WDTM_WDTPS_0400MS		(2<<3)|1
		#define		WDT_WDTM_WDTPS_0800MS		(1<<3)|1
		#define		WDT_WDTM_WDTPS_1600MS		(0<<3)|1
sfr SFR_TMXC		= 0xBC;
		#define		TMXC_EN				BIT7
//		SFR_TMXC_TMXE_BIT		BIT SFR_TMXC^7;
//		SFR_TMXC_TMXM_BIT		BIT SFR_TMXC^5;
//		SFR_TMXC_RSTCPU_BIT		BIT SFR_TMXC^4;
		#define		TMXC_PRE_DIV64		3
		#define		TMXC_PRE_DIV16		2
		#define		TMXC_PRE_DIV4		1
		#define		TMXC_PRE_DIV1		0
sfr SFR_TMX			= 0xBD;
sfr SFR_SPH			= 0xBE;
//sfr SFR_BF			= 0xBF;
;==[0xC0]============================================================
sfr SFR_AIF			= 0xC0;
		#define		AIF_EXIOF_BIT		BIT4
		#define		AIF_I2CF_BIT		BIT2
		#define		AIF_SPIRXF_BIT		BIT1
		#define		AIF_SPITXF_BIT		BIT0
		#define		AIF_EXIOF			4
		#define		AIF_I2CF			2
		#define		AIF_SPIRXF			1
		#define		AIF_SPITXF			0
 		SFR_AIF_EXIOF_BIT		BIT SFR_AIF.AIF_EXIOF;
		SFR_AIF_I2CF_BIT		BIT SFR_AIF.AIF_I2CF;
		SFR_AIF_SPIRXF_BIT		BIT SFR_AIF.AIF_SPIRXF;
		SFR_AIF_SPITXF_BIT		BIT SFR_AIF.AIF_SPITXF;
sfr SFR_NMIE		= 0xC1;
//		SFR_NMIE_TMXWKE_BIT		BIT SFR_NMIE^5;
//		SFR_NMIE_EXIOWKE_BIT	BIT SFR_NMIE^4;
//		SFR_NMIE_USBWKE_BIT		BIT SFR_NMIE^3;
//		SFR_NMIE_I2CWKE_BIT		BIT SFR_NMIE^2;
//		SFR_NMIE_SPIWKE_BIT		BIT SFR_NMIE^0;
sfr SFR_NMIF		= 0xC2;
//		SFR_NMIF_TMXWKF_BIT		BIT SFR_NMIF^5;
//		SFR_NMIF_EXIOWKF_BIT	BIT SFR_NMIF^4;
//		SFR_NMIF_USBWKF_BIT		BIT SFR_NMIF^3;
//		SFR_NMIF_I2CWKF_BIT		BIT SFR_NMIF^2;
//		SFR_NMIF_SPIWKF_BIT		BIT SFR_NMIF^0;
sfr SFR_IF			= 0xC3;
		#define		IF_TMXF_BIT		BIT6	
		#define		IF_ADF_BIT		BIT5
		#define		IF_UARTF_BIT	BIT4
		#define		IF_T1F_BIT		BIT3
		#define		IF_T0F_BIT		BIT1
		#define		IF_TMXF			6	
		#define		IF_ADF			5
		#define		IF_UARTF		4
		#define		IF_T1F			3
		#define		IF_T0F			1
		SFR_IF_TMXF_BIT			BIT SFR_IF.IF_TMXF;
		SFR_IF_ADF_BIT			BIT SFR_IF.IF_ADF;
		SFR_IF_UARTF_BIT		BIT SFR_IF.IF_UARTF;
		SFR_IF_T1F_BIT			BIT SFR_IF.IF_T1F;
		SFR_IF_T0F_BIT			BIT SFR_IF.IF_T0F;
sfr SFR_ISPC		= 0xC4;
sfr SFR_ISPCLK		= 0xC5;
sfr SFR_ISPPAGE		= 0xC6;
sfr SFR_ISPULK		= 0xC7;
sfr SFR_EGEN0		= 0xC8;
sfr SFR_EGEN1		= 0xC9;
sfr SFR_EGTG0		= 0xCA;
sfr SFR_EGTG1		= 0xCB;
sfr SFR_EGFG0		= 0xCC;
sfr SFR_EGFG1		= 0xCD;
//sfr SFR_CE			= 0xCE;
//sfr SFR_CF			= 0xCF;
;==[0xD0]============================================================
sfr SFR_PSW			= 0xD0;
		#define		PSW_REG_BANK3			(3<<3)
		#define		PSW_REG_BANK2			(2<<3)
		#define		PSW_REG_BANK1			(1<<3)
		#define		PSW_REG_BANK0			(0<<3)
sfr SFR_PSW1		= 0xD1;
		SFR_PSW1_NEGATIVE_BIT		BIT SFR_PSW1.5;
		SFR_PSW1_ZERO_BIT			BIT SFR_PSW1.1;
//sfr SFR_D2			= 0xD2;
//sfr SFR_D3			= 0xD3;
//sfr SFR_D4			= 0xD4;
//sfr SFR_D5			= 0xD5;
//sfr SFR_D6			= 0xD6;
//sfr SFR_D7			= 0xD7;
//sfr SFR_D8			= 0xD8;
//sfr SFR_D9			= 0xD9;
//sfr SFR_DA			= 0xDA;
//sfr SFR_DB			= 0xDB;
//sfr SFR_DC			= 0xDC;
//sfr SFR_DD			= 0xDD;
//sfr SFR_DE			= 0xDE;
//sfr SFR_DF			= 0xDF;
;==[0xE0]============================================================
sfr SFR_ACC			= 0xE0;
sfr SFR_VER			= 0xE1;
sfr SFR_UCTR		= 0xE2;
		#define		UCTR_SMOD0_BIT			7	
		#define		UCTR_UARTEN_BIT			6
		#define		UCTR_SMOD0				(1<<UCTR_SMOD0_BIT)
		#define		UCTR_UARTEN				(1<<UCTR_UARTEN_BIT)
		SFR_UCTR_SMOD0_BIT	BIT SFR_UCTR.7;
		SFR_UCTR_UARTEN_BIT	BIT SFR_UCTR.6;
sfr SFR_SCON		= 0xE3;
		#define		SCON_FE_SM0_BIT			7
		#define		SCON_SM1_BIT			6
		#define		SCON_SM2_BIT			5
		#define		SCON_REN_BIT			4
		#define		SCON_TB8_BIT			3
		#define		SCON_RB8_BIT			2
		#define		SCON_TI_BIT				1
		#define		SCON_RI_BIT				0
		#define		SCON_FE_SM0				(1<<7)
		#define		SCON_SM1				(1<<6)
		#define		SCON_SM2				(1<<5)
		#define		SCON_REN				(1<<4)
		#define		SCON_TB8				(1<<3)
		#define		SCON_RB8				(1<<2)
		#define		SCON_TI					(1<<1)
		#define		SCON_RI					(1<<0)
		SFR_SCON_FE_SM0_BIT		BIT 	SFR_SCON.SCON_FE_SM0_BIT;
		SFR_SCON_SM1_BIT		BIT 	SFR_SCON.SCON_SM1_BIT;
		SFR_SCON_SM2_BIT		BIT 	SFR_SCON.SCON_SM2_BIT;
		SFR_SCON_REN_BIT		BIT 	SFR_SCON.SCON_REN_BIT;
		SFR_SCON_TB8_BIT		BIT 	SFR_SCON.SCON_TB8_BIT;
		SFR_SCON_RB8_BIT		BIT 	SFR_SCON.SCON_RB8_BIT;
		SFR_SCON_TI_BIT			BIT 	SFR_SCON.SCON_TI_BIT;
		SFR_SCON_RI_BIT			BIT 	SFR_SCON.SCON_RI_BIT;
sfr SFR_SBUF		= 0xE4;
sfr SFR_UBDIVL		= 0xE5;
sfr SFR_UBDIVH		= 0xE6;
//sfr SFR_E7			= 0xE7;
sfr SFR_AIE			= 0xE8;
		SFR_AIE_EXIOE_BIT	BIT SFR_AIE.4;
		SFR_AIE_I2CE_BIT	BIT SFR_AIE.2;
		SFR_AIE_SPIRXE_BIT	BIT SFR_AIE.1;
		SFR_AIE_SPITXE_BIT	BIT SFR_AIE.0;
//sfr SFR_E9		= 0xE9;
sfr SFR_CLKCON		= 0xEA;
sfr SFR_CLKWM		= 0xEB;
sfr SFR_OSCL		= 0xEC;
sfr SFR_OSCH		= 0xED;
//sfr SFR_EE			= 0xEE;
//sfr SFR_EF			= 0xEF;
;==[0xF0]============================================================
sfr SFR_B			= 0xF0;
sfr SFR_I2CADR		= 0xF1;
//		SFR_I2CADR_I2CE_BIT	BIT SFR_I2CADR^7;
sfr SFR_I2CIR		= 0xF2;
	#define		SFR_I2CIR_STWR		7//BIT SFR_I2CIR^7;
	#define		SFR_I2CIR_STRD		6//BIT SFR_I2CIR^6;
	#define		SFR_I2CIR_EMP		5//BIT SFR_I2CIR^5;
	#define		SFR_I2CIR_FUL		4//BIT SFR_I2CIR^4;
	#define		SFR_I2CIR_STOP		3//BIT SFR_I2CIR^3;
sfr SFR_I2CTRG		= 0xF3;
		SFR_I2CTRG_LASTSTRD_BIT		BIT 	SFR_I2CTRG^7;
		SFR_I2CTRG_LASTSTWR_BIT		BIT 	SFR_I2CTRG^6;
		SFR_I2CTRG_TRGSTRD_BIT		BIT 	SFR_I2CTRG^1;
		SFR_I2CTRG_TRGSTWR_BIT		BIT 	SFR_I2CTRG^0;
sfr SFR_I2CCNT		= 0xF4;
		SFR_I2CCNT_RST_BIT			BIT 	SFR_I2CCNT^2;
		SFR_I2CCNT_EMP_BIT			BIT 	SFR_I2CCNT^1;
		SFR_I2CCNT_FUL_BIT			BIT 	SFR_I2CCNT^0;
sfr SFR_I2CBUF		= 0xF5;
//sfr SFR_F6			= 0xF6;
sfr SFR_AIPH		= 0xF7;
//		SFR_AIPH_EXIOP_BIT	BIT SFR_AIPH^4;
//		SFR_AIPH_USBP_BIT	BIT SFR_AIPH^3;
//		SFR_AIPH_I2CP_BIT	BIT SFR_AIPH^2;
//		SFR_AIPH_SPIRXP_BIT	BIT SFR_AIPH^1;
//		SFR_AIPH_SPITXP_BIT	BIT SFR_AIPH^0;
sfr SFR_AIPL		= 0xF8;
//		SFR_AIPL_EXIOP_BIT	BIT SFR_AIPL^4;
//		SFR_AIPL_USBP_BIT	BIT SFR_AIPL^3;
//		SFR_AIPL_I2CP_BIT	BIT SFR_AIPL^2;
//		SFR_AIPL_SPIRXP_BIT	BIT SFR_AIPL^1;
//		SFR_AIPL_SPITXP_BIT	BIT SFR_AIPL^0;
//sfr SFR_F9			= 0xF9;
//sfr SFR_FA			= 0xFA;
//sfr SFR_FB			= 0xFB;
//sfr SFR_FC			= 0xFC;
//sfr SFR_FD			= 0xFD;
//sfr SFR_FE			= 0xFE;
//sfr SFR_FF			= 0xFF;


#endif //__A8008_DEF_H__