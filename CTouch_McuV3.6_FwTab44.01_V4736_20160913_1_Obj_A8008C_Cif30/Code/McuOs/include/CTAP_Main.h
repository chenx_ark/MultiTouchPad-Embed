;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTAP_MAIN_H__
#define	__CTAP_MAIN_H__

EXTERN	CODE	(CTAP_Init)
EXTERN	CODE	(CTAP_CoordinateProcessing)
EXTERN	CODE	(CTAP_DeviceCtrlResetHandler)
EXTERN	CODE	(CTAP_DeviceCtrlPwrDnHandler)


#endif	//__CTAP_MAIN_H__