;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTPWR_MAIN_H__
#define	__CTPWR_MAIN_H__

EXTERN	CODE	(CTPWR_PowerDnEna)
EXTERN	CODE	(CTNMI_IntSr)

#endif	//__CTPWR_MAIN_H__