;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTADADJ_MAIN_H__
#define	__CTADADJ_MAIN_H__

//EXTERN	CODE	(CTADADJ_AutoAdAdjInit)
//EXTERN	CODE	(CTADADJ_AutoAdAdjustment)
//EXTERN	CODE	(CTADADJ_AdAdjSensitivityDetect)

#endif	//__CTADADJ_MAIN_H__