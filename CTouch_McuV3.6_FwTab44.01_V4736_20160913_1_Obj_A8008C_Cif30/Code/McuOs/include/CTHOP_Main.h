;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTHOP_MAIN_H__
#define	__CTHOP_MAIN_H__

EXTERN	CODE	(CTHOP_FreqHopInit)
#if(CTMOS_DISABLE_FREQ_HOPPING == 0)
EXTERN	CODE	(CTHOP_FreqHoppingProcessing)
#endif//(CTMOS_DISABLE_FREQ_HOPPING == 0)
EXTERN	CODE	(CTHOP_UpdateP1)
//EXTERN	CODE	(CTHOP_UpdateP12)
//EXTERN	CODE	(CTHOP_SensingFreqTab)
//EXTERN	CODE	(CTHOP_CheckBestRefFinished)
EXTERN	CODE	(CTHOP_UpdateNoiseTabAndCurrFreqIdx)

#endif	//__CTHOP_MAIN_H__