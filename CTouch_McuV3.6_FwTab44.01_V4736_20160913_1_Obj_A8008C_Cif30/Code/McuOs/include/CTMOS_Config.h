//;******************************************************************************
//;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
//;* Author: Benson Shen (Jun. 20, 2013)
//;* Copyright (C) 2013, Sitronix Tech. Corp.
//;******************************************************************************

#ifndef	__CTMOS_CONFIG_H__
#define	__CTMOS_CONFIG_H__

//====[A8008 Chip Setting]===================================================
#define			A8008_IC_REVISION					A8008_IC_REV_C//0:A8008A,1:A8008B,3:A8008C
	#define			A8008_IC_REV_A						0
	#define			A8008_IC_REV_B					 	1
	#define			A8008_IC_REV_C					 	3

#if(A8008_IC_REVISION==A8008_IC_REV_A)
	#define		MOS_CODE_VER						0x0103//A8008A,NoSupport
#elif(A8008_IC_REVISION==A8008_IC_REV_B)
	#define		MOS_CODE_VER						0x0203//A8008B
#else//(A8008_IC_REVISION==A8008_IC_REV_C)
	#define		MOS_CODE_VER						0x0306//A8008C
#endif//(A8008_IC_REVISION==A8008_IC_REV_x)

#define 	FW_VERSION							0x01
#define 	FW_BUILD_VER						0x002B


//====[Device Mode Selecttion]==========================================
#define			DEVICE_MODE						DEVICE_NORMAL_MODE
	#define		DEVICE_NORMAL_MODE				0x00
	#define		DEVICE_AUTO_TUNE_MODE			0x01
	#define		DEVICE_SIMULATION_MODE			0x40
	#define		DEVICE_DRIVER_MODE				0x41
	#define		DEVICE_DEBUG_MODE				0x42
	#define		DEVICE_DEBUG_IN_PHONE_MODE		0x43


//====[I2C Protocol Option]=============================================
#define			DEVICE_I2C_PROTOCOL				DEVICE_I2C_REPLACE
	#define		DEVICE_I2C_SITRONIX_A			1
	#define		DEVICE_I2C_GOODIX				0x20
	#define		DEVICE_I2C_FOCOL				0x40
	#define		DEVICE_I2C_REPLACE				0xFF

#if(DEVICE_I2C_PROTOCOL==DEVICE_I2C_REPLACE)
	#define			CTMOS_I2C_REPL_C_IF			1
	#define			CTMOS_I2C_PROTOCOL_MODIFY	1
	#define			REPLACE_I2C_PROTOCOL		REPLACE_I2C_SITRONIX_A
#else//(DEVICE_I2C_PROTOCOL==DEVICE_I2C_REPLACE)
	#define			CTMOS_I2C_REPL_C_IF			0
	#define			CTMOS_I2C_PROTOCOL_MODIFY	0
	#define			REPLACE_I2C_PROTOCOL		0
#endif//(DEVICE_I2C_PROTOCOL==DEVICE_I2C_REPLACE)
		#define			REPLACE_I2C_SITRONIX_A	0x01
		#define			REPLACE_I2C_GOODIX		0x20
		#define			REPLACE_I2C_FOCOL		0x40
		#define			REPLACE_I2C_NONE		0x00


//====[Touch Algo Type Option]================================================
#define			TOUCH_ALGO_TYPE					(CTMOS_I2C_PROTOCOL_MODIFY<<7|CTMOS_KEY_RX_INDEPENDENT<<6|CTMOS_COORD_REPORT_MODE<<5|TOUCH_ALGO_ID)
#define			TOUCH_ALGO_ID					ALGO_N_TOUCH_1D_E_TYPE
	#define		ALGO_N_TOUCH_BAR_BAR			1
	#define		ALGO_TRIANGLE					2
	#define		ALGO_N_TOUCH_CROSS_PATTERN		3
	#define		ALGO_N_TOUCH_1D_E_TYPE			4


//====[Function Option]====================================================
#if(A8008_IC_REVISION==A8008_IC_REV_A)
	#define			CTMOS_CURR_TAB_VER				0x27
#elif(A8008_IC_REVISION==A8008_IC_REV_B)
	#define			CTMOS_CURR_TAB_VER				0x31   
#else//(A8008_IC_REVISION==A8008_IC_REV_C)
	#define			CTMOS_CURR_TAB_VER				0x44   
#endif//(A8008_IC_REVISION==A8008_IC_REV_A)
#define			CTMOS_KEY_RX_INDEPENDENT		0x0
#define			CTMOS_COORD_REPORT_MODE			0x0
#define			CTHOP_MAX_NUM_FREQ				6//15//10
#define			CTMOS_FREQ_HOP_RETRY_EN
#define			CTHOP_NEW_FREQ_HOP_EN
#define			CTMOS_LCM_NOISE_SUPPRESSION_EN	2
#define			CTMOS_PFLT_FINDER				0
#define			CTMOS_ISP_TOUCH_FW2_EN			1//0:DisTF61, 1:Ver0.1, 2:Ver0.2
#define			CTMOS_I2C_PTC_SHRINK			0//1:I2C Code Shrink For SWU
#define			CTMOS_IDLE_SCAN_MODE			0//0:Self+Mutual, 1:Self(Can't Use To LCM Noise Environment)

//====[Debug Option]====================================================
#define			CTMOS_IO_CMD_FORCE_EN			0
//#define			CTUART_DEBUG
//#define			CTMOS_NO_ROM_CHECK_SUM
#define			CTMOS_GEN_FW_PARAM_TAB
//#define			CTMOS_GPIO_OUT_DEBUG
//#define			CTMOS_P11_IS_INT
//#define			CTMOS_DEVELOP_NOISE_RAW
//#define			CTMOS_RAWDATA_DETECT_ANALYSIS_EN
//#define			CTMOS_EVALUATION_EN


//====[Code Shrink Option]==============================================
#define			CTMOS_TIMERX_FROM_24M_DIS		0x1
#define			CTMOS_24MHZ_FROM_FWTAB_DIS		0x1
#define			CTMOS_REPORT_LIMIT_DIS			0x1

//DevSubPage
#define			CTMOS_DEVELOP_ALL_DIS			0x1
//FwModule[0]
#define			CTMOS_RAWDATA_DETECT_EN			0x0
#define			CTMOS_COMMAND_IO_READ_EN		0x0
#define			CTMOS_COMMAND_IO_WRITE_EN		0x0
//FwModule[1]									   
#define			CTMOS_KEY_SCAN_MODULE_EN		0x0
//FwModule[2]

#if((DEVICE_I2C_PROTOCOL==DEVICE_I2C_SITRONIX_A)||(DEVICE_I2C_PROTOCOL==DEVICE_I2C_GOODIX))
	//0:DisSWU, 1:SWU By Key, 2:SWU By AASensor
	#define			CTMOS_SMART_WAKEUP_EN			0x0
#else//(DEVICE_I2C_PROTOCOL==DEVICE_I2C_FOCOL)
	#define			CTMOS_SMART_WAKEUP_EN			0x0
#endif//(DEVICE_I2C_PROTOCOL==DEVICE_I2C_SITRONIX_A)											   

//--------------20130926----------------kchsu
#define 	CTMOS_DISABLE_FREQ_HOPPING				0
#define 	CTMOS_DISABLE_NOISE_FLAG				0
#define 	CTMOS_DISABLE_NOISE_THROW				0
#define 	CTMOS_DISABLE_NOISE_DATA_DETECT			0
#define 	CTMOS_CAL_NOISE_STATE					0

//-------------------------------------------

//====[Function Option]==
#ifndef	CTMOS_EVALUATION_EN
	#define			RUN_ALGORITHM
#endif//CTMOS_EVALUATION_EN


//====[A8008 Chip Setting]===================================================
#define			A8008_CHIP_ID						6
#define			A8008_MAX_RX_CH						12
#define			A8008_MAX_TX_CH						21
#define			A8008_MAX_KEY_CH					4
#define			A8008_MAX_NUM_TOUCH					10//5
#define			INT_REG_BANK3						0x03


//====[CSTouchLib]=========================================
#define CFG_CTALG_NONE							0
#define CFG_CTALG_CSMTOUCH						1
#define CFG_CTALG_CSMTOUCH_VERIFY				2

#define CFG_CTALG_BUILD							(CFG_CTALG_CSMTOUCH)

//====[Security]=========================================
#define		REPLACE_SECURITY			0

#endif	//__CTMOS_CONFIG_H__
