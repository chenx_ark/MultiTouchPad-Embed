;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************
#ifndef	__CTMOS_DATA_H__
#define	__CTMOS_DATA_H__


;====[CTMOS Stack Addr]===================
EXTERN		EDATA		(StackAddr)



//;====[CTI2C MAIN]=========================	
EXTERN	BIT		(gbtCTI2C_WrFlag)
EXTERN	BIT		(gbtCTI2C_RdFlag)
EXTERN	BIT		(gbtCTI2C_CmdAddrByteFlag)
EXTERN	BIT		(gbtCTI2C_WrOneByteFlag)
EXTERN	BIT		(gbtCTI2C_ReadTagFinishFlag)
EXTERN	BIT		(gbtCTI2C_UnlockDevelopFlag)
EXTERN	DATA	(gtCTI2C_HighByteCmdAddr)
EXTERN	DATA	(gtCTI2C_CmdAddr)
EXTERN	DATA	(gtCTI2C_ByteCnt)
EXTERN	EDATA	(getCTI2C_RegMapMem)
//EXTERN	IDATA	(gtCTI2C_BackupIsrRegister)

//;====[CTI2C CMD]=========================
EXTERN	EDATA	(getCTI2C_CmdMutuAlgoRawLineNum)
EXTERN	EDATA	(getCTI2C_IspPassWordCnt)


//;====[CTTMX MAIN]=========================
#if(CTMOS_COORD_REPORT_MODE==2)
EXTERN	BIT		(gbtCTTMX_Timex2msUnitFlag)
#endif//(CTMOS_COORD_REPORT_MODE==2)
EXTERN	BIT		(gbtCTTMX_Timex4msUnitFlag)
EXTERN	DATA	(gtCTTMX_ScanPeriodCnt)
//EXTERN	DATA	(gtCTTMX_CoordReportPeriodCnt)

#if(CTMOS_SMART_WAKEUP_EN==2)||(CTMOS_IDLE_SCAN_MODE==1)
EXTERN	DATA	(gtCTTMX_SmartWakeupModeMonitorCnt)
#endif//(CTMOS_SMART_WAKEUP_EN==2)

EXTERN	EDATA	(gewCTTMX_ScanIdleCnt)
//EXTERN	EDATA	(geCTTMX_CoordReportPeriodValue)
EXTERN	EDATA	(geCTTMX_ScanNormalPeriodValue)
EXTERN	EDATA	(geCTTMX_ScanIdlePeriodValue)

EXTERN	EDATA	(geCTTMX_IdleCaliCnt)

//;====[CTSCAN MAIN]========================
EXTERN	BIT		(gbtCTSCAN_ScanFrameFlag)
EXTERN	BIT		(gbtCTSCAN_ScanIdx)
EXTERN	BIT		(gbtCTSCAN_RawIdx)
EXTERN	BIT		(gbCTSCAN_RxTxRotateFlag)
//EXTERN	BIT		(gbCTSCAN_KeyRxSelFlag)
EXTERN	BIT		(gbtCTSCAN_KeyRawBufFull_Flag)
EXTERN	BIT		(gbCTSCAN_KeyScanFlag)

#if(A8008_IC_REVISION==A8008_IC_REV_C)
//EXTERN	BIT		(gbtCTSCAN_SelfScanFlag)
EXTERN	BIT		(gbCTSCAN_DeveSendSelfRawDataFlag)
#endif//(A8008_IC_REVISION==A8008_IC_REV_C)

#if(CTMOS_SMART_WAKEUP_EN==0)
#else//(CTMOS_SMART_WAKEUP_EN==0)
EXTERN	BIT		(gbCTSCAN_SmartWakeupEnFlag)
EXTERN	BIT		(gbCTSCAN_SmartWakeupScanFlag)
EXTERN	BIT		(gbCTSCAN_SmartWakeupRawFlag)
#endif//(CTMOS_SMART_WAKEUP_EN==0)

#if(CTMOS_IDLE_SCAN_MODE==1)
EXTERN	BIT		(gbCTSCAN_SmartWakeupScanFlag)
EXTERN	BIT		(gbCTSCAN_SmartWakeupRawFlag)
#endif//(CTMOS_IDLE_SCAN_MODE==1)

EXTERN	BIT		(gbtCTSCAN_SetScanActiveFlag)


EXTERN	DATA	(gCTSCAN_MaxEvenChNum)
EXTERN	DATA	(gCTSCAN_MaxOddChNum)
EXTERN	DATA	(gtCTSCAN_FreeScanNumFrame)
EXTERN	DATA	(gtCTSCAN_RawDataRemain0)
EXTERN	DATA	(gtCTSCAN_RawDataRemain1)
//EXTERN	DATA	(gtCTSCAN_RawDataLineNum0)
//EXTERN	DATA	(gtCTSCAN_RawDataLineNum1)
EXTERN	DATA	(gtCTSCAN_AlgoLineNumCnt)
EXTERN	DATA	(gtCTSCAN_ScanMode)
//EXTERN	DATA	(gtCTSCAN_SIdx)
//EXTERN	DATA	(gtCTSCAN_RIdx)
EXTERN	DATA	(gtCTSCAN_AlgoRawRemain)
EXTERN	DATA	(gtCTSCAN_GetRawDataType)
//EXTERN	DATA	(gtCTSCAN_RawDataType0)
//EXTERN	DATA	(gtCTSCAN_RawDataType1)
EXTERN  EDATA	(getCTSCAN_ScanDataBufSts)
//20130926 kchsu//EXTERN  EDATA	(gewCTSCAN_Tests)
//EXTERN  EDATA	(gewCTSCAN_NextP12)
EXTERN  EDATA	(gewCTSCAN_P12)
//EXTERN  EDATA	(get4CTSCAN_RawDataBufSts)
//EXTERN  EDATA	(gewtCTSCAN_NoiseDataBuf0)
//EXTERN  EDATA	(gewtCTSCAN_NoiseDataBuf1)

//EXTERN  EDATA	(geCTSCAN_OtherTxChIndex)
//EXTERN  EDATA	(geCTSCAN_ReplaceTxChIndex)
//EXTERN  EDATA	(geCTSCAN_RawMuTxCnt)

#if(CTMOS_KEY_SCAN_MODULE_EN==1)
EXTERN  EDATA	(geCTSCAN_KeyChTabMapping)
EXTERN  EDATA	(gewCTSCAN_KeyRawData)
#endif//(CTMOS_KEY_SCAN_MODULE_EN==1)

//EXTERN  EDATA	(gewtCTSCAN_NextGnd0RawBuf0)		
//EXTERN  EDATA	(gewtCTSCAN_NextGnd0RawBuf1)
//EXTERN  EDATA	(gewtCTSCAN_CurrRaw0Buf0)
//EXTERN  EDATA	(gewtCTSCAN_CurrRaw0Buf1)
//EXTERN  EDATA	(gewtCTSCAN_CurrGndBuf0)
//EXTERN  EDATA	(gewtCTSCAN_CurrGndBuf1)

#if(A8008_IC_REVISION==A8008_IC_REV_C)
EXTERN  EDATA	(getCTSCAN_SelfRawData0)
EXTERN  EDATA	(getCTSCAN_SelfRawData1)
//EXTERN  EDATA	(getCTSCAN_SelfBackupRawData0)
#endif//(A8008_IC_REVISION==A8008_IC_REV_C)

//#if(CTMOS_HOP_COMP_EN==2)//debug
//EXTERN  EDATA	(geCTSCAN_AdCmopDebugRam)
//#endif//(CTMOS_HOP_COMP_EN==2)//debug

//;====[CTDEVE MAIN]=========================
EXTERN	BIT		(gbCTDEVE_DevelopPageFlag)
EXTERN	BIT		(gbtCTDEVE_RawTransFinishFlag)
//EXTERN	BIT		(gbtCTDEVE_DeveRawOutFinishFlag)


//;====[CTMOS MAIN]=========================	
EXTERN	BIT		(gbCTMOS_FwErrFlag)
EXTERN	BIT		(gbtCTMOS_ScanIdleFlag)	
EXTERN	BIT		(gbCTMOS_ExeCoordTransFlag)
EXTERN	BIT		(gbCTMOS_SysPowerDnFlag)
EXTERN	BIT		(gbCTMOS_IntRisingTrigFlag)
EXTERN	BIT		(gbCTMOS_Temp0Flag)
//EXTERN	BIT		(gbCTMOS_Temp1Flag)
//EXTERN	BIT		(gbCTMOS_Temp2Flag)
//EXTERN	BIT		(gbCTMOS_Temp3Flag)
EXTERN	BIT		(gbtCTMOS_Temp0Flag)

EXTERN	DATA	(gtCTMOS_I2cDevStatusReg)//0x01
EXTERN	DATA	(gtCTMOS_I2cDevCtlReg)//0x02
EXTERN	DATA	(gCTMOS_EmiChkDevCtl)
EXTERN	DATA	(gCTMOS_I2cTimeToIdleValue)//0x03
EXTERN	DATA	(gtCTMOS_I2cResX)//0x04~0x06
EXTERN	DATA	(gtCTMOS_I2cResY)//0x04~0x06
EXTERN	DATA	(gtCTMOS_I2cMiscCtl)//0xF1
EXTERN	DATA	(gtCTMOS_I2cPageNumber)//0xFF

EXTERN	DATA	(gCTMOS_AlgProcCnt)
EXTERN	DATA	(gtCTMOS_ReportCoordSts)



EXTERN	DATA	(gCTMOS_NumChX)
EXTERN	DATA	(gCTMOS_NumChY)
EXTERN	DATA	(gCTMOS_NumChKey)
//EXTERN	DATA	(gCTMOS_NumChProx)
EXTERN	DATA	(gCTMOS_NumChTx)
EXTERN	DATA	(gCTMOS_NumChRx)
EXTERN	DATA	(gCTMOS_WithKeyNumChTx)


EXTERN	DATA	(gCTMOS_Temp0Ram)
//EXTERN	DATA	(gCTMOS_Temp1Ram)
//EXTERN	DATA	(gCTMOS_Temp2Ram)
EXTERN	DATA	(gtCTMOS_Temp0Ram)
EXTERN	DATA	(gtCTMOS_Temp1Ram)

//EXTERN IDATA	(giCTMOS_IDataTempBuff)//20130925
//EXTERN	EDATA	(getCTMOS_DataTempBuff)

EXTERN	EDATA	(geCTMOS_TimerxCntTrimValue)
EXTERN	EDATA	(geCTMOS_TimerxCntSwkIdleValue)
EXTERN	EDATA	(geCTMOS_GpioStatus)
EXTERN	EDATA	(geCTMOS_Temp0Ram)//forDebug


;====[ISP Main]====
EXTERN	BIT		(gbtCTISP_SoftWareIspFlag)


//;====[DebugOnly]====
//EXTERN	EDATA	(getCTMOS_DebugRam0)

;====[Freq Hop]====
//EXTERN	BIT		(gbCTHOP_ExeFreqHopProcFlag)
EXTERN	BIT		(gbCTHOP_RetryAllFreqFlag)
EXTERN	BIT		(gbCTHOP_TestFreqFlag)
//EXTERN	BIT		(gbCTHOP_IncP1Flag)
EXTERN	BIT		(gbCTHOP_NoiseFlag) //20131020 debug//
//EXTERN	BIT		(gCTHOP_FirstFrameNoUpdateRawFreqIdxFlag)
EXTERN	BIT		(gbCTHOP_DummyScanFlag)
//EXTERN	BIT		(gbCTHOP_GenBestRefFlag)
//EXTERN	BIT		(gbtCTHOP_ScanFreqSelFlag)
//EXTERN	BIT		(gbCTHOP_AlgGenBestRefReadyFlag)
//EXTERN	BIT		(gbCTHOP_ScanBestFreqIdxFlag)


EXTERN	DATA	(gCTHOP_CurrFreqIdx)
EXTERN	DATA	(gCTHOP_P1FreqIdx)
EXTERN	DATA	(gCTHOP_RawFreqIdx)
EXTERN	DATA	(gCTHOP_ScanFreqIdx)
EXTERN	DATA	(gCTHOP_TestFreqIdx)
EXTERN	DATA	(gCTHOP_MinNoiseFreqIdx)
//EXTERN	DATA	(gCTHOP_P1Hopping)

//EXTERN	DATA	(gCTHOP_MaxNoise)		//Added by CL_Chen (2013/09/24)

//EXTERN	EDATA	(geCTHOP_NoiseRxCntJitter)
EXTERN	EDATA	(geCTHOP_NoiseStrength)
EXTERN	EDATA	(geCTHOP_NoNoiseCnt)
EXTERN	EDATA	(geCTHOP_FreqHopNoiseTh)
EXTERN	EDATA	(gewCTHOP_MaxNoise)
//EXTERN	EDATA	(gewCTHOP_NextFreqMaxNoise)
//EXTERN	EDATA	(geCTHOP_NextRawFreqIdx)
//EXTERN	EDATA	(geCTHOP_RawDataDetectCnt)

//EXTERN	DATA	(gCTHOP_RawDebounceCnt)
//EXTERN	EDATA	(gewCTHOP_PreviousDetectRaw0)
//EXTERN	EDATA	(gewCTHOP_PreviousDetectRaw1)
//EXTERN	EDATA	(gewCTHOP_RawDebounceCnt0)
//EXTERN	EDATA	(gewCTHOP_RawDebounceCnt1)

//EXTERN	EDATA	(geCTHOP_PreviousFreqIdx)
EXTERN	EDATA	(gewtCTHOP_FreqHopRetryCnt)

//EXTERN	EDATA	(getCTHOP_NextScanFreqIdx)
//EXTERN	EDATA	(getCTHOP_NextScanFreqCnt)
//EXTERN	EDATA	(geCTHOP_DelayOutCurrFreqIdx)
//EXTERN	EDATA	(geCTHOP_BestFreqIdx)

;====[AP Coord]====
#if(DEVICE_I2C_PROTOCOL!=DEVICE_I2C_REPLACE)
EXTERN  EDATA	(geCTMOS_CoordTempBuf)
#endif//(CTMOS_I2C_REPL_C_IF==0)&&(DEVICE_I2C_PROTOCOL==DEVICE_I2C_REPLACE)
EXTERN  EDATA	(geCTMOS_MaxNumTouch)
EXTERN	BIT		(gbtCTMOS_I2cCoordBufFullFlag)
EXTERN	BIT		(gbCTAP_CoordForceTrigFlag)
//EXTERN	BIT		(gbtCTAP_ReportCoordFlag)
EXTERN	BIT		(gbCTAP_CoordProcFinishFlag)
EXTERN	BIT		(gbCTAP_WithTouchFlag)
EXTERN	BIT		(gbCTAP_I2cCoordBufChkFlag)
//#if(CTMOS_REPORT_LIMIT_DIS==0)
//EXTERN	BIT		(gbtCTAP_2ReportFlag)
//#endif//(CTMOS_REPORT_LIMIT_DIS==0)

EXTERN	BIT		(gbCTAP_TouchPalmFlag)

;====[Timer0]====
EXTERN  EDATA	(geCTT0_ForIntReleaseTimer0HiCnt)

;====[Develop Direct Access Addr]====
EXTERN  EDATA	(CTMOS_CtlParamRamAddr)
EXTERN  EDATA	(geCTDEVE_ScanCtl)
EXTERN	EDATA	(gedwCTMOS_FwTabAccePtr)
//EXTERN  EDATA	(gewCTSCAN_RawTargetValue)



;====[MOS Touch Fw Tab]====
EXTERN  EDATA	(geCTMOS_TouchFwTabRam)


;====[MOS Main]====
EXTERN	BIT		(gbCTMOS_RawDataDetectNoiseFlag)
//EXTERN	BIT		(gbCTMOS_DebounceDetectNoiseFlag)


//#ifdef	CTMOS_RAWDATA_DETECT_ANALYSIS_EN
//EXTERN  EDATA	(gewCTMOS_NoNoiseDetectCnt)
//EXTERN  EDATA	(gewCTMOS_FrameCnt)
//EXTERN  EDATA	(gewCTMOS_DebounceCnt)
//EXTERN  EDATA	(gewCTMOS_PreviRawData)
//EXTERN  EDATA	(gewCTMOS_RawDataDelta)
//EXTERN  EDATA	(gewCTMOS_NoDetectCnt)
//#endif//CTMOS_RAWDATA_DETECT_ANALYSIS_EN

#if(DEVICE_I2C_PROTOCOL==DEVICE_I2C_REPLACE)
EXTERN  EDATA	(geCTMOS_BackupRegister)
EXTERN  EDATA	(getCTI2C_ReplaceBackReg0)
EXTERN  EDATA	(getCTI2C_ReplaceBackReg1)
EXTERN  EDATA	(getCTI2C_ReplaceBackReg2)
////EXTERN  IDATA	(giCTMOS_BackupRegister)
//geCTMOS_BackupRegister		EQU		EDATA		0x0000DC
#endif//(DEVICE_I2C_PROTOCOL==DEVICE_I2C_REPLACE)


//EXTERN	BIT		(gbtCTSCAN_NextSensingFlag)
EXTERN	BIT		(gbtCTHOP_FindCurrFreqIdxFlag)
EXTERN	DATA	(gtCTHOP_HopRawFreqIdx)
//EXTERN  EDATA	(getCTHOP_NextFreqIdx)
//EXTERN  EDATA	(getCTHOP_NextRawFreqIdx)
EXTERN  EDATA	(gewCTHOP_MaxNextNoise)
//EXTERN  EDATA	(geCTHOP_NextRestoreTxIdx)
EXTERN  EDATA	(geCTHOP_GetMaxNextNoiseRdy)

EXTERN  EDATA	(getCTSCAN_NextRawData0)
EXTERN  EDATA	(getCTSCAN_NextRawData1)
EXTERN  EDATA	(getCTSCAN_PrevNextRawData0)
EXTERN  EDATA	(getCTSCAN_PrevNextRawData1)


#ifdef	CTMOS_EVALUATION_EN
EXTERN	BIT		(gbtCTMOS_EvaRawDataReady)

//====[Evaluation]=========================================
#define		CTMOS_ONE_TRANS_SIZE						120//OneTransSize
CTMOS_EvaRam						EQU		EDATA		0x000400
getwCTMOS_EvaDataSend2HostSize		EQU		EDATA		CTMOS_EvaRam//2Bytes//0x400
getwCTMOS_EvaDataSend2HostCnt		EQU		EDATA		getwCTMOS_EvaDataSend2HostSize+2//2Bytes//0x402
__gAlgNumLines						EQU		EDATA		getwCTMOS_EvaDataSend2HostCnt+2//2Bytes//0x404
__gAlgLineIndex						EQU		EDATA		__gAlgNumLines+2//12Bytes//0x406
__gAlgHalfFrameBuff					EQU		EDATA		__gAlgLineIndex+12//64Bytes//0x412

CTMOS_EvaTmpRaw						EQU		EDATA		__gAlgHalfFrameBuff+((21+6)*6*2)//HalfFrame//0x000480
geCTMOS_OneLineBuf					EQU		EDATA		CTMOS_EvaTmpRaw//64Bytes//0x480
geCTMOS_EvaSelfRaw					EQU		EDATA		geCTMOS_OneLineBuf+64//12*2=24Bytes//0x
geCTMOS_EvaMutuRaw					EQU		EDATA		geCTMOS_EvaSelfRaw+24//12*(21+2)*2=552Bytes//
geCTMOS_Eva2HostData				EQU		EDATA		geCTMOS_EvaMutuRaw+552//

//Eva Data One Transaction Format, Max Size=125
//Command ID(1) = 9->ContuneEvaData,10->LastEvaData
//Package Size(1) = Total Size.
//Mode H(1) = .
//Mode L(1) = .
//==[CTMOS_EvaData Content]==S
//Mutu Data(Rx*(Tx+6)) = 
//Self Data(Rx) =
//Key Data(Key) =
//Curr Freq Index(1) =
//Curr Noise Max Noise(2) =
//No Noise Cnt(6) =
//Noise Strength(24) =
//Next Freq Index(1) =
//Next Max Noise(12) =
//Coord(2) =
//==[CTMOS_EvaData Content]==E
//Check Sum(1) =
#endif//CTMOS_EVALUATION_EN

#endif	//__CTMOS_DATA_H__