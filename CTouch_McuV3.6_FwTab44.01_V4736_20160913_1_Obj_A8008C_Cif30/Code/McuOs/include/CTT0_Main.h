;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTT0_MAIN_H__
#define	__CTT0_MAIN_H__

extern	CODE	(CTT0_Init)
extern	CODE	(CTT0_Timer0IntSr)

#endif	//__CTT0_MAIN_H__