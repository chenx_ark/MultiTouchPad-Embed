;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTSCAN_PARAMCONFIG_DEF_H__
#define	__CTSCAN_PARAMCONFIG_DEF_H__

//------------------[added by kchsu 20130927 ]----------------------
#define 	CTSCAN_NOISE_SCAN_NUM_TX					2//2 Only
#define 	CTSCAN_NOISE_SCAN_TX_INDEX					0x15
//------------------------------------------------------------------
#define		CTSCAN_NEXT_SCAN_NUM_TX						6

#define		CTSCAN_RAW_TARGET_VALUE						0x0400

#define		CTSCAN_RAW_DATA_STS_EMPTY					0
#define		CTSCAN_RAW_DATA_STS_FULL					1

#define		CTSCAN_SCAN_DATA_STS_EMPTY					0
#define		CTSCAN_SCAN_DATA_STS_SENSING				1
#define		CTSCAN_SCAN_DATA_STS_FULL					2

#define		CTSCAN_NOISE_DETECTION_MODE0				0
#define		CTSCAN_TOUCH_RAW_DATA_MODE0					1
#define		CTSCAN_NOISE_DETECTION_MODE1				2
#define		CTSCAN_TOUCH_RAW_DATA_MODE1					3

//#define		CTSCAN_SCAN_DATA_GND_MODE0					0
//#define		CTSCAN_SCAN_DATA_RAW_MODE0					1
//#define		CTSCAN_SCAN_DATA_GND_MODE1					2
//#define		CTSCAN_SCAN_DATA_RAW_MODE1					3

//#define		CTSCAN_NEXT_GND0_RAW_SCAN_MODE0				0
//#define		CTSCAN_CURR_RAW0_SCAN_MODE0					1
//#define		CTSCAN_CURR_RAW1_GND_SCAN_MODE0				2
//#define		CTSCAN_NEXT_GND1_SCAN_MODE0					3
//#define		CTSCAN_NEXT_GND0_RAW_SCAN_MODE1				4
//#define		CTSCAN_CURR_RAW0_SCAN_MODE1					5
//#define		CTSCAN_CURR_RAW1_GND_SCAN_MODE1				6
//#define		CTSCAN_NEXT_GND1_SCAN_MODE1					7
//#define		CTSCAN_SCAN_MODE_IS_OVERFLOW				8
							
#define		CTSCAN_SELF0_RAW_SCAN_MODE					0
#define		CTSCAN_SELF1_RAW_SCAN_MODE					1
#define		CTSCAN_NOISE0_IDX0_SCAN_MODE				2
#define		CTSCAN_NOISE1_IDX0_SCAN_MODE				3
#define		CTSCAN_NOISE0_IDX1_SCAN_MODE				4
#define		CTSCAN_NOISE1_IDX1_SCAN_MODE				5
#define		CTSCAN_NOISE0_IDX2_SCAN_MODE				6
#define		CTSCAN_NOISE1_IDX2_SCAN_MODE				7
#define		CTSCAN_NOISE0_IDX3_SCAN_MODE				8
#define		CTSCAN_NOISE1_IDX3_SCAN_MODE				9
#define		CTSCAN_NOISE0_IDX4_SCAN_MODE				10
#define		CTSCAN_NOISE1_IDX4_SCAN_MODE				11
#define		CTSCAN_NOISE0_IDX5_SCAN_MODE				12
#define		CTSCAN_NOISE1_IDX5_SCAN_MODE				13
#define		CTSCAN_MUTU0_RAW_SCAN_MODE					14
#define		CTSCAN_MUTU1_RAW_SCAN_MODE					15

#define		CTSCAN_SELF_RAW_SCAN_MODE					0
#define		CTSCAN_NOISE_IDX0_SCAN_MODE					1
#define		CTSCAN_NOISE_IDX1_SCAN_MODE					2
#define		CTSCAN_NOISE_IDX2_SCAN_MODE					3
#define		CTSCAN_NOISE_IDX3_SCAN_MODE					4
#define		CTSCAN_NOISE_IDX4_SCAN_MODE					5
#define		CTSCAN_NOISE_IDX5_SCAN_MODE					6
#define		CTSCAN_MUTU_RAW_SCAN_MODE					7




#define		CTSCAN_NOT_IN_SCAN_INT						0
#define		CTSCAN_IN_SCAN_INT							1

#define		CTSCAN_NO_GET_RAW							0
#define		CTSCAN_GET_NORMAL_RAW_ONLY					1
#define		CTSCAN_GET_NOISE_RAW_ONLY					2
#define		CTSCAN_GET_NORMAL_AND_NOISE_RAW				3
#define		CTSCAN_GET_NEXT_NOISE_RAW_ONLY				4
//#define		CTSCAN_GET_ALL_RAW							7

//#define		CTSCAN_NOISE_RAW_OF_EVEN_RX_FRAME			0
//#define		CTSCAN_NOISE_RAW_OF_ODD_RX_FRAME			1
//#define		CTSCAN_TOUCH_RAW_OF_EVEN_RX_FRAME			2
//#define		CTSCAN_TOUCH_RAW_OF_ODD_RX_FRAME			3

#define		CTFT_TOUCH_FW_TAB_HEADER_ROM_H				0x00FF
#define		CTFT_TOUCH_FW_TAB_HEADER_ROM_L				0x3F00
//#define		CTFT_TOUCH_FW_TAB_HEADER_RAM_H				0x0000

//#define		CTFT_DEVELOP_RAW_RAM_TAB0					0x0400
//#define		CTFT_DEVELOP_RAW_RAM_TAB1				   	0x0600

	





//typedef struct CTFT6_TouchFwTabHeader{                                               
//==[Touhc Fw Table Header]==																
#define		CTFT_TABLE_TAG						0										//unsigned char TableTag[4];//Table tag = "STFW".
#define		CTFT_PARAM_TAB_VER					CTFT_TABLE_TAG+4						//unsigned char ParamTabVersion[2]; 
#define		CTFT_CHIP_ID						CTFT_PARAM_TAB_VER+2					//unsigned char ChipId;                
#define		CTFT_RESERVED						CTFT_CHIP_ID+1							//unsigned char Reserved;//Reserved = 0        
#define		CTFT_CHECK_SUM_OFFSET				CTFT_RESERVED+1							//unsigned char ChecksumOffset;                
#define		CTFT_SYS_ENV_PARAM_TAB_OFFSET		CTFT_CHECK_SUM_OFFSET+1					//unsigned char SysEnvParamTabOffset;                
#define		CTFT_ALG_PARAM_TAB_OFFSET			CTFT_SYS_ENV_PARAM_TAB_OFFSET+1	    	//unsigned char AlgParamTabOffset;                
#define		CTFT_SCAN_PARAM_TAB_OFFSET			CTFT_ALG_PARAM_TAB_OFFSET+1			    //unsigned char ScanParamTabOffset;                
#define		CTFT_RX_CH_MAPPING_OFFSET			CTFT_SCAN_PARAM_TAB_OFFSET+1		    //unsigned char RxChMappingOffset;                
#define		CTFT_TX_CH_MAPPING_OFFSET			CTFT_RX_CH_MAPPING_OFFSET+1			    //unsigned char TxChMappingOffset;                
#define		CTFT_KEY_CH_MAPPING_OFFSET			CTFT_TX_CH_MAPPING_OFFSET+1				//unsigned char KeyChMappingOffset;
#define		CTFT_FW_MISC_PARAM_TAB_OFFSET		CTFT_KEY_CH_MAPPING_OFFSET+1			//unsigned char FwMiscParamTabOffset;                
#define		CTFT_I2C_PROTOCOL_TAB_OFFSET		CTFT_FW_MISC_PARAM_TAB_OFFSET+1	    		//unsigned char I2cProtocolTabOffset;                
#if(A8008_IC_REVISION==A8008_IC_REV_A)
	#define		CTFT_RAW_COMP_TAB_OFFSET			CTFT_I2C_PROTOCOL_TAB_OFFSET+1	
#else//(A8008_IC_REVISION==A8008_IC_REV_A)
	#define		CTFT_RESERVED1						CTFT_I2C_PROTOCOL_TAB_OFFSET+1			//unsigned char RawCompTabOffset;
#endif//(A8008_IC_REVISION==A8008_IC_REV_A)
//}CTFT6_TOUCH_FW_TAB_HEADER; 



//typedef struct CTFT6_SysEnvParamTab{ 													 
#define		CTFT_SYSTEM_CLOCK_FREQ				0										//unsigned long SystemClockFreq;    //The unit is Hz. 
#define		CTFT_COMMU_INTERFACE				CTFT_SYSTEM_CLOCK_FREQ+4				//unsigned char CommuInterface; 
#define		CTFT_SYS_ENV_CTL					CTFT_COMMU_INTERFACE+1					//unsigned char SysEnvCtl; 
#define		CTFT_LDO_LVR						CTFT_SYS_ENV_CTL+1						//unsigned char LdoLVR; 
#define		CTFT_TRIM_OSC						CTFT_LDO_LVR+1							//unsigned char TrimOsc[2]; 
#define		CTFT_TRIM_LDO_V18					CTFT_TRIM_OSC+2							//unsigned char TrimLdoV18; 
#define		CTFT_TRIM_LDO_V25					CTFT_TRIM_LDO_V18+1						//unsigned char TrimLdoV25; 
#define		CTFT_LDO_V25_OFFSET					CTFT_TRIM_LDO_V25+1						//unsigned char LdoV25Offset; 
#define		CTFT_SYS_MISC_CTL					CTFT_LDO_V25_OFFSET+1					//unsigned char MiscCtl; 
#define		CTFT_SYS_RESERVED					CTFT_SYS_MISC_CTL+1						//unsigned char Reserved[2];      //Reserved[4] = {0,0,0,0} 
//}CTFT6_SYS_ENV_PARAM_TAB;



//typedef struct CTFT6_ScanParamTab{   
#define		CTFT_SENSING_RATE					0										//unsigned char SensingRate; 
#define		CTFT_RES_X							CTFT_SENSING_RATE+1						//short ResX;            //LCM X resolution. 
#define		CTFT_RES_Y							CTFT_RES_X+2							//short ResY;            //LCM Y resolution. 
#define		CTFT_NUM_CH_X						CTFT_RES_Y+2							//unsigned char NumChX; 
#define		CTFT_NUM_CH_Y						CTFT_NUM_CH_X+1							//unsigned char NumChY; 
#define		CTFT_NUM_CH_KEY						CTFT_NUM_CH_Y+1							//unsigned char NumChKey;        
#define		CTFT_SCAN_RESERVED0					CTFT_NUM_CH_KEY+1						//unsigned char Reserved;       // Reserved = 0. Reserved for NumChProximity. 
#define		CTFT_SENSING_CTL					CTFT_SCAN_RESERVED0+1					//unsigned char SensingCtl[4];        
#define		CTFT_OSNR_CTL						CTFT_SENSING_CTL+4						//unsigned short OsnrCtl;        
#if(A8008_IC_REVISION==A8008_IC_REV_A)
	#define		CTFT_NOISE_DETECTION_CTL			CTFT_OSNR_CTL+2							//unsigned short NoiseDetectionCtl;   
	#define		CTFT_NOISE_DETECTION_DELAY			CTFT_NOISE_DETECTION_CTL+2				//unsigned short NoiseDecectionDelay; 
	#define		CTFT_NOISE_HOPPING_DETECTION		CTFT_NOISE_DETECTION_DELAY+2			//unsigned short NoiseHoppingDetection; 
	#define		CTFT_DUMMY_SCAN_CNT					CTFT_NOISE_HOPPING_DETECTION+2			//unsigned short DummyScanCnt;        
#else//(A8008_IC_REVISION==A8008_IC_REV_B,C)
	#define		CTFT_MUTU_CAP_OFFSET				CTFT_OSNR_CTL+2							//unsigned short CapOffset;   
	#define		CTFT_RESERVED1_2					CTFT_MUTU_CAP_OFFSET+2					//unsigned short Reserved1[2];  
	#define		CTFT_DUMMY_SCAN_CNT					CTFT_RESERVED1_2+4						//unsigned short DummyScanCnt;        
#endif//(A8008_IC_REVISION==A8008_IC_REV_A)
#define		CTFT_P12							CTFT_DUMMY_SCAN_CNT+2					//unsigned short P12;        
#define		CTFT_NCLK_CTL						CTFT_P12+2								//unsigned short NclkCtl;        
#define		CTFT_PHASE_CNT						CTFT_NCLK_CTL+2							//unsigned short PhaseCnt;        
#define		CTFT_ANALOG_CTL						CTFT_PHASE_CNT+2						//unsigned short AnalogCtl;        
#define		CTFT_RX_CINT_PARAM					CTFT_ANALOG_CTL+2						//unsigned char RxCintParam[CTFT_MAX_NUM_RX_CH]; 
//#define		CTFT_P_FLT							CTFT_RX_CINT_PARAM+12					//unsigned char FreqHopP1[5]; 
#if(A8008_IC_REVISION==A8008_IC_REV_A)
	#define		CTFT_FREQ_HOP_P1					CTFT_RX_CINT_PARAM+12					//unsigned char FreqHopP1[5]; 
#elif(A8008_IC_REVISION==A8008_IC_REV_B)
	#define		CTFT_P_FLT							CTFT_RX_CINT_PARAM+12					//unsigned char FreqHopP1[5]; 
#else//(A8008_IC_REVISION==A8008_IC_REV_C)
	#define		CTFT_P_FLT							CTFT_RX_CINT_PARAM+12					//unsigned char P_FLT[6]; 
	#define		CTFT_SELF_CAP_OFFSET				CTFT_P_FLT+6							//unsigned short SelfCapOffset; 
	#define		CTFT_SELF_P12						CTFT_SELF_CAP_OFFSET+2					//unsigned short SelfP12; 
	#define		CTFT_SELF_RX_CINT_PARAM				CTFT_SELF_P12+2							//unsigned char SelfRxCintParam[CTFT6_MAX_NUM_RX_CH]; 
	#define		CTFT_SELF_P_FLT						CTFT_SELF_RX_CINT_PARAM+12				//unsigned char SelfP_FLT; 
#endif//(A8008_IC_REVISION==A8008_IC_REV_A)


//}CTFT6_SCAN_PARAM_TAB;                

       
       
//typedef struct CTFT6_FwMiscParamTab{
#define		CTFT_FW_MISC_CTL					0										//unsigned char MiscCtl; 
#define		CTFT_I2C_ADDR						CTFT_FW_MISC_CTL+1						//unsigned char I2cAddr; 
#define		CTFT_I2C_PROTOCOL_SELECT			CTFT_I2C_ADDR+1							//unsigned char I2cProtocolSelect;
#define		CTFT_FREQ_HOP_STEP					CTFT_I2C_PROTOCOL_SELECT+1				//unsigned char FreqHopStep;
#define		CTFT_FREQ_HOP_NOISE_TH				CTFT_FREQ_HOP_STEP+1					//unsigned char FreqHopNoiseTh;
#define		CTFT_ALG_BIN_CONFIG_VER				CTFT_FREQ_HOP_NOISE_TH+1						//unsigned char AlgBinConfigVersion; 
#define		CTFT_CUSTOMER_ID					CTFT_ALG_BIN_CONFIG_VER+1				//unsigned char CustomerId; 
#define		CTFT_CUST_PROJ_ID					CTFT_CUSTOMER_ID+1						//unsigned char CustProjId; 
#define		CTFT_TP_SUPPLIER_ID					CTFT_CUST_PROJ_ID+1						//unsigned char TpSupplierId; 
#define		CTFT_TP_SUPP_PROJ_ID				CTFT_TP_SUPPLIER_ID+1	
#define		CTFT_MAX_NUM_TOUCH					CTFT_TP_SUPP_PROJ_ID+1					//unsigned char MaxNumTouch;	
//}CTFT6_FW_MISC_PARAM_TAB;         



//typedef struct CTFT6_FtParamTab{ 
#define		CTFT_FT_KEY_COORD_0					0 
#define		CTFT_FT_KEY_COORD_1					CTFT_FT_KEY_COORD_0+3					//CTFT_FT_KEY_2_COORD FtKeyCoord1; 
#define		CTFT_FT_KEY_COORD_2					CTFT_FT_KEY_COORD_1+3					//CTFT_FT_KEY_2_COORD FtKeyCoord2; 
#define		CTFT_FT_KEY_COORD_3					CTFT_FT_KEY_COORD_2+3					//CTFT_FT_KEY_2_COORD FtKeyCoord3; 
#define		CTFT_FT_FW_LIB_VERSION_H			CTFT_FT_KEY_COORD_3+3					//unsigned char FtFwLibVersionH; 
#define		CTFT_FT_FW_LIB_VERSION_L			CTFT_FT_FW_LIB_VERSION_H+1				//unsigned char FtFwLibVersionL; 
#define		CTFT_FT_CHIP_VENSOR_ID				CTFT_FT_FW_LIB_VERSION_L+1				//unsigned char FtChipVendorId; 
#define		CTFT_FT_INT_MODE					CTFT_FT_CHIP_VENSOR_ID+1				//unsigned char FtIntMode; 
#define		CTFT_FT_POWER_MODE					CTFT_FT_INT_MODE+1						//unsigned char FtPowMode; 
#define		CTFT_FT_FIRMWARE_ID					CTFT_FT_POWER_MODE+1					//unsigned char FtFirmwareId; 
#define		CTFT_FT_RUNNING_STATE				CTFT_FT_FIRMWARE_ID+1					//unsigned char FtRunningState; 
#define		CTFT_FT_CTPM_VENDOR_ID				CTFT_FT_RUNNING_STATE+1					//unsigned char FtCTPMVendorId; 
#define		CTFT_FT_RELEASE_CODE_ID				CTFT_FT_CTPM_VENDOR_ID+1				//unsigned char FtReleaseCodeId; 
//}CTFT6_FT_PARAM_TAB;		



//typedef struct CTFT6_GtParamTab{ 

//}CTFT6_GT_PARAM_TAB;	




#endif	//__CTSCAN_PARAMCONFIG_DEF_H__