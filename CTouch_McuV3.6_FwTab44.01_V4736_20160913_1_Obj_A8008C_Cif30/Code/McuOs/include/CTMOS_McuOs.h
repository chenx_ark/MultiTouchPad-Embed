;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTMOS_MCUOS_H__
#define	__CTMOS_MCUOS_H__

EXTERN	CODE	(CTMOS_McuOsMainReset)

////EXTERN	CODE	(CTMOS_PowerDnWakeup)
EXTERN		CODE	(CTMOS_GetNoiseInfo)
////EXTERN		CODE	(CTMOS_GetSensingCtlPtr)
EXTERN		CODE	(CTMOS_GetScanParamTabPtr)
EXTERN		CODE	(CTMOS_GetDiv1024SysClkFreq)

//#if(DEVICE_I2C_PROTOCOL == DEVICE_I2C_GOODIX)
EXTERN		CODE	(CTMOS_GetIntCtrlBit)
//#endif//(DEVICE_I2C_PROTOCOL == DEVICE_I2C_GOODIX)


#endif	//__CTMOS_MCUOS_H__