;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTI2C_CMD_H__
#define	__CTI2C_CMD_H__


#if((CTMOS_DEVELOP_ALL_DIS==0)||(CTMOS_COMMAND_IO_READ_EN==1)||(CTMOS_COMMAND_IO_WRITE_EN==1))
	EXTERN	CODE	(CTI2C_CmdIoPortHandler)
#endif//((CTMOS_DEVELOP_ALL_DIS==0)||(CTMOS_COMMAND_IO_READ_EN==1)||(CTMOS_COMMAND_IO_WRITE_EN==1))

#if(CTMOS_DEVELOP_ALL_DIS==0)
	EXTERN	CODE	(CTI2C_DataOutPacketHandler)
#endif//(CTMOS_DEVELOP_ALL_DIS==0)


#endif	//__CTI2C_CMD_H__