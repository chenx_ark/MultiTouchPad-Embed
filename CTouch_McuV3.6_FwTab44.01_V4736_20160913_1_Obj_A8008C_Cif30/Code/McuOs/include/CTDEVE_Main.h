;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTDEVE_MAIN_H__
#define	__CTDEVE_MAIN_H__

#if(CTMOS_DEVELOP_ALL_DIS==0)
EXTERN	CODE	(CTDEVE_DevePageInit)
EXTERN	CODE	(CTDEVE_DevePageProcessing)
EXTERN	CODE	(CTDEVE_DataOutPacketHandler)
EXTERN	CODE	(CTDEVE_GetRawDataPecketFinishHandler)
#endif//(CTMOS_DEVELOP_ALL_DIS==0)

#endif	//__CTDEVE_MAIN_H__