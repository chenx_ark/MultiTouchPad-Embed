;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef __A8008_EXREG_DEF_H__
#define __A8008_EXREG_DEF_H__


;;====[RX Parameter Table Cint]==============================
RX_PARAM_CINT_TAB_ADDR		EQU		EDATA		0x002000//0xC00


;;====[TX Index Table]==============================
TX_INDEX_TAB_ADDR			EQU		EDATA		0x002018//0xC18//


;;====[Raw Data Table]===============================
RAW_DATA_TAB0_ADDR			EQU		EDATA		0x002200//0xA00//0x002200
RAW_DATA_TAB1_ADDR			EQU		EDATA		0x002400//0xB00//0x002400


;;====[Adc Data Table]===============================
ADC_DATA_TAB0_ADDR			EQU		EDATA		0x002600
ADC_DATA_TAB1_ADDR			EQU		EDATA		0x002700


;;====[Register Ram]=================================
SFR_EX_NDFDAT				EQU		EDATA		0x00FF12
SFR_EX_OCTL					EQU		EDATA		0x00FF40
SFR_EX_NCTL					EQU		EDATA		0x00FF42//A8008A,A8008B
SFR_EX_OFFSET				EQU		EDATA		0x00FF42//A8008C
SFR_EX_NDLY					EQU		EDATA		0x00FF44//A8008A,A8008B
SFR_EX_NHOP					EQU		EDATA		0x00FF46//A8008A,A8008B
SFR_EX_DCNT					EQU		EDATA		0x00FF48
SFR_EX_P12					EQU		EDATA		0x00FF4A
SFR_EX_NCLK					EQU		EDATA		0x00FF4C
SFR_EX_CNT					EQU		EDATA		0x00FF4E
SFR_EX_CTL0					EQU		EDATA		0x00FF50
SFR_EX_MTXCNT				EQU		EDATA		0x00FF52
SFR_EX_PFLT					EQU		EDATA		0x00FF54













#endif //__A8008_EXREG_DEF_H__