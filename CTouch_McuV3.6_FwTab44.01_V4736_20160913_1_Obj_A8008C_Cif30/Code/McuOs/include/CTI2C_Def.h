;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTI2C_DEF_H__
#define	__CTI2C_DEF_H__


#define 	I2C_REG_STATUS_DEVICE				(BIT3|BIT2|BIT1|BIT0)
	#define		I2C_REG_STATUS_DEVICE_NORMAL				0x0
	#define		I2C_REG_STATUS_DEVICE_INIT					0x1
	#define		I2C_REG_STATUS_DEVICE_ERROR					0x2
	#define		I2C_REG_STATUS_DEVICE_AUTO_TUNING			0x3
	#define		I2C_REG_STATUS_DEVICE_IDLE					0x4
	#define		I2C_REG_STATUS_DEVICE_PWR_DN				0x5
	#define		I2C_REG_STATUS_DEVICE_BOOT_ROM				0x6	
	#define		I2C_REG_STATUS_DEVICE_ICRC					0x7	

#define 	I2C_REG_STATUS_ERROR				(BIT7|BIT6|BIT5|BIT4)
	#define 	I2C_REG_STATUS_ERROR_NO_ERROR				0x0
	#define 	I2C_REG_STATUS_ERROR_INVALID_ADDR			0x1
	#define 	I2C_REG_STATUS_ERROR_INVALID_VALUE			0x2
	#define 	I2C_REG_STATUS_ERROR_INVALID_PLATFORM		0x3
	#define 	I2C_REG_STATUS_ERROR_DEV_NOT_FOUND			0x4
	#define 	I2C_REG_STATUS_ERROR_STACK_OVERFLOW			0x5		
	#define 	I2C_REG_STATUS_ERROR_CONFIG_TABLE_VER		0x6// ConfigTableVer and ConfigTableChkSum is Err
	#define 	I2C_REG_STATUS_ERROR_INVALID_TOUCH_FW2		0x7


//#define		I2C_REG_TIMEOUT_TO_IDLE_VALUE					0xFF//0x08
                                                                           

#endif	//__CTI2C_DEF_H__