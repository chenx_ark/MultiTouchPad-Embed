;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTTMX_MAIN_H__
#define	__CTTMX_MAIN_H__

EXTERN	CODE	(CTTMX_Init)
EXTERN	CODE	(CTTMX_IntSr)
EXTERN	CODE	(CTTMX_TimeToIdleRegProcessing)
EXTERN	CODE	(CTTMX_TimeoutToIdleHandler)

#endif	//__CTTMX_MAIN_H__