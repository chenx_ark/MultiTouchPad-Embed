;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTMOS_FUNC_H__
#define	__CTMOS_FUNC_H__

EXTERN	CODE	(CTAP_GetScanMode)
EXTERN	CODE	(CTSCAN_SetFreeScanNumFrame)

#if(DEVICE_I2C_PROTOCOL == DEVICE_I2C_SITRONIX_A)
EXTERN	CODE	(CTAP_GetPenOpMode)
#endif//(DEVICE_I2C_PROTOCOL == DEVICE_I2C_SITRONIX_A)

#endif	//__CTMOS_FUNC_H__