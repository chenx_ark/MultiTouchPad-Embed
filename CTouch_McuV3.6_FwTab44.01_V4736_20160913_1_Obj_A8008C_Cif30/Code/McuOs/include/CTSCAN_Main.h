;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTSCAN_MAIN_H__
#define	__CTSCAN_MAIN_H__

EXTERN	CODE	(CTSCAN_Init)
EXTERN	CODE	(CTSCAN_ScanIntSr)
EXTERN	CODE	(CTSCAN_ScanProcessing)
EXTERN	CODE	(CTSCAN_FreeRawDataBuf)
EXTERN	CODE	(CTSCAN_TrigTouchSensorSensing)
//EXTERN	CODE	(CTSCAN_GetRawDataBufSts)
//EXTERN	CODE	(CTSCAN_SetRawDataBufSts)

EXTERN	CODE	(CTSCAN_GetRawDataRemain)
EXTERN	CODE	(CTSCAN_SetRawDataRemain)
//EXTERN	CODE	(CTSCAN_GetRawDataLineNum)
//EXTERN	CODE	(CTSCAN_SetRawDataLineNum)
EXTERN	CODE	(CTSCAN_GetSensingRawData)
EXTERN	CODE	(CTMOS_GetKeyRawWithLcmSuppression)
//EXTERN	CODE	(CTSCAN_GetSensingNoiseRawData)
//EXTERN	CODE	(CTSCAN_DevGetSensingRawData)
//EXTERN	CODE	(CTSCAN_GetRawDataType)
EXTERN	CODE	(CTSCAN_GetScanDataBufSts)
EXTERN	CODE	(CTSCAN_SetScanDataBufSts)

//EXTERN	CODE	(CTSCAN_GetKeyRawData)
EXTERN	CODE	(CTSCAN_GetRxChannelMappingPtr)

#endif	//__CTSCAN_MAIN_H__