;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTMOS_FWINF_H__
#define	__CTMOS_FWINF_H__

EXTERN	CODE	(CTMOS_TouchFwInfo)
EXTERN	CODE	(CTMOS_TouchFwInfoEnd)

#endif	//__CTMOS_FWINF_H__