;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTI2C_GOODIX_DEF_H__
#define	__CTI2C_GOODIX_DEF_H__

//Ref GT960 Spec
#define		GT_H_CMD_0x00							0x00
#define		GT_H_CMD_0x80							0x80
#define		GT_H_CMD_0x81							0x81

#define		GT_WR_CMD_COMMAND						0x8040
#define		GT_WR_CMD_RESERVED						0x8041
#define		GT_WR_CMD_PROX_EN						0x8042

#define		GT_X_OUTPUT_MAX_LOW						0x8048
#define		GT_X_OUTPUT_MAX_HIGH					0x8049
#define		GT_Y_OUTPUT_MAX_LOW						0x804A
#define		GT_Y_OUTPUT_MAX_HIGH					0x804B

#define		GT_RD_CMD_SMART_WAKEUP_ID				0x814B

#define		GT_I2C_CMD_START						0x100
GT_EX_WR_CMD_COMMAND				EQU	EDATA		0x02+GT_I2C_CMD_START
	#define		GT_EX_WR_CMD_COMMAND_COORD			0x00	
	#define		GT_EX_WR_CMD_COMMAND_RAW			0x01
	#define		GT_EX_WR_CMD_COMMAND_RESET			0x02
	#define		GT_EX_WR_CMD_COMMAND_BASEUPDATE		0x03
	#define		GT_EX_WR_CMD_COMMAND_CALIBRATION	0x04
	#define		GT_EX_WR_CMD_COMMAND_POWERDOWN		0x05
	#define		GT_EX_WR_CMD_COMMAND_CHARGMODE		0x06
	#define		GT_EX_WR_CMD_COMMAND_CHARGEXIT		0x07
	#define		GT_EX_WR_CMD_COMMAND_DOZEMODE		0x08
GT_EX_WR_CMD_RESERVED				EQU	EDATA		0x03+GT_I2C_CMD_START
GT_EX_WR_CMD_PROX_EN				EQU	EDATA		0x04+GT_I2C_CMD_START

GT_EX_TIMEOUT_TO_IDLE_VALUE			EQU	EDATA		0x05+GT_I2C_CMD_START

GT_EX_0x4220_CMD					EQU	EDATA		0x20+GT_I2C_CMD_START
GT_EX_0x4221_CMD					EQU	EDATA		0x21+GT_I2C_CMD_START
GT_EX_0x4222_CMD					EQU	EDATA		0x22+GT_I2C_CMD_START
GT_EX_0x4223_CMD					EQU	EDATA		0x23+GT_I2C_CMD_START

GT_EX_PID_1							EQU	EDATA		0x40+GT_I2C_CMD_START
GT_EX_PID_2							EQU	EDATA		0x41+GT_I2C_CMD_START
GT_EX_PID_3							EQU	EDATA		0x42+GT_I2C_CMD_START
GT_EX_PID_4							EQU	EDATA		0x43+GT_I2C_CMD_START
GT_EX_FW_VER_L						EQU	EDATA		0x44+GT_I2C_CMD_START
GT_EX_FW_VER_H						EQU	EDATA		0x45+GT_I2C_CMD_START
GT_EX_X_COORD_RESOL_L				EQU	EDATA		0x46+GT_I2C_CMD_START
GT_EX_X_COORD_RESOL_H				EQU	EDATA		0x47+GT_I2C_CMD_START
GT_EX_Y_COORD_RESOL_L				EQU	EDATA		0x48+GT_I2C_CMD_START
GT_EX_Y_COORD_RESOL_H				EQU	EDATA		0x49+GT_I2C_CMD_START

GT_EX_VID							EQU	EDATA		0x4A+GT_I2C_CMD_START
GT_EX_CMD_SMART_WAKEUP_ID			EQU	EDATA		0x4B+GT_I2C_CMD_START
//GT_EX_CMD_RESERVED0					EQU	EDATA		0x4B+GT_I2C_CMD_START

GT_EX_CMD_RESERVED1					EQU	EDATA		0x4C+GT_I2C_CMD_START
GT_EX_CMD_RESERVED2					EQU	EDATA		0x4D+GT_I2C_CMD_START

GT_EX_TD_STATUS						EQU	EDATA		0x4E+GT_I2C_CMD_START

GT_EX_TRACK1_ID						EQU	EDATA		0x4F+GT_I2C_CMD_START
GT_EX_TOUCH1_XL						EQU	EDATA		0x50+GT_I2C_CMD_START
GT_EX_TOUCH1_XH						EQU	EDATA		0x51+GT_I2C_CMD_START
GT_EX_TOUCH1_YL						EQU	EDATA		0x52+GT_I2C_CMD_START
GT_EX_TOUCH1_YH						EQU	EDATA		0x53+GT_I2C_CMD_START
GT_EX_TOUCH1_SIZE_L					EQU	EDATA		0x54+GT_I2C_CMD_START
GT_EX_TOUCH1_SIZE_H					EQU	EDATA		0x55+GT_I2C_CMD_START
GT_EX_TOUCH1_RESERVED				EQU	EDATA		0x56+GT_I2C_CMD_START

GT_EX_TRACK2_ID						EQU	EDATA		0x57+GT_I2C_CMD_START
GT_EX_TOUCH2_XL						EQU	EDATA		0x58+GT_I2C_CMD_START
GT_EX_TOUCH2_XH						EQU	EDATA		0x59+GT_I2C_CMD_START
GT_EX_TOUCH2_YL						EQU	EDATA		0x5A+GT_I2C_CMD_START
GT_EX_TOUCH2_YH						EQU	EDATA		0x5B+GT_I2C_CMD_START
GT_EX_TOUCH2_SIZE_L					EQU	EDATA		0x5C+GT_I2C_CMD_START
GT_EX_TOUCH2_SIZE_H					EQU	EDATA		0x5D+GT_I2C_CMD_START
GT_EX_TOUCH2_RESERVED				EQU	EDATA		0x5E+GT_I2C_CMD_START

GT_EX_TRACK3_ID						EQU	EDATA		0x5F+GT_I2C_CMD_START
GT_EX_TOUCH3_XL						EQU	EDATA		0x60+GT_I2C_CMD_START
GT_EX_TOUCH3_XH						EQU	EDATA		0x61+GT_I2C_CMD_START
GT_EX_TOUCH3_YL						EQU	EDATA		0x62+GT_I2C_CMD_START
GT_EX_TOUCH3_YH						EQU	EDATA		0x63+GT_I2C_CMD_START
GT_EX_TOUCH3_SIZE_L					EQU	EDATA		0x64+GT_I2C_CMD_START
GT_EX_TOUCH3_SIZE_H					EQU	EDATA		0x65+GT_I2C_CMD_START
GT_EX_TOUCH3_RESERVED				EQU	EDATA		0x66+GT_I2C_CMD_START

GT_EX_TRACK4_ID						EQU	EDATA		0x67+GT_I2C_CMD_START
GT_EX_TOUCH4_XL						EQU	EDATA		0x68+GT_I2C_CMD_START
GT_EX_TOUCH4_XH						EQU	EDATA		0x69+GT_I2C_CMD_START
GT_EX_TOUCH4_YL						EQU	EDATA		0x6A+GT_I2C_CMD_START
GT_EX_TOUCH4_YH						EQU	EDATA		0x6B+GT_I2C_CMD_START
GT_EX_TOUCH4_SIZE_L					EQU	EDATA		0x6C+GT_I2C_CMD_START
GT_EX_TOUCH4_SIZE_H					EQU	EDATA		0x6D+GT_I2C_CMD_START
GT_EX_TOUCH4_RESERVED				EQU	EDATA		0x6E+GT_I2C_CMD_START                                                                   

GT_EX_TRACK5_ID						EQU	EDATA		0x6F+GT_I2C_CMD_START
GT_EX_TOUCH5_XL						EQU	EDATA		0x70+GT_I2C_CMD_START
GT_EX_TOUCH5_XH						EQU	EDATA		0x71+GT_I2C_CMD_START
GT_EX_TOUCH5_YL						EQU	EDATA		0x72+GT_I2C_CMD_START
GT_EX_TOUCH5_YH						EQU	EDATA		0x73+GT_I2C_CMD_START
GT_EX_TOUCH5_SIZE_L					EQU	EDATA		0x74+GT_I2C_CMD_START
GT_EX_TOUCH5_SIZE_H					EQU	EDATA		0x75+GT_I2C_CMD_START
GT_EX_TOUCH5_RESERVED				EQU	EDATA		0x76+GT_I2C_CMD_START

GT_EX_SPECIAL0xC0_CMD				EQU	EDATA		0xC0+GT_I2C_CMD_START//Cfg Specified Cmd	
GT_EX_SPECIAL0xC1_CMD				EQU	EDATA		0xC1+GT_I2C_CMD_START
GT_EX_SPECIAL0xC2_CMD				EQU	EDATA		0xC2+GT_I2C_CMD_START
GT_EX_SPECIAL0xC3_CMD				EQU	EDATA		0xC3+GT_I2C_CMD_START
GT_EX_SPECIAL0xC4_CMD				EQU	EDATA		0xC4+GT_I2C_CMD_START
GT_EX_SPECIAL0xC5_CMD				EQU	EDATA		0xC5+GT_I2C_CMD_START
GT_EX_SPECIAL0xC6_CMD				EQU	EDATA		0xC6+GT_I2C_CMD_START
GT_EX_SPECIAL0xC7_CMD				EQU	EDATA		0xC7+GT_I2C_CMD_START
GT_EX_SPECIAL0xC8_CMD				EQU	EDATA		0xC8+GT_I2C_CMD_START
GT_EX_SPECIAL0xC9_CMD				EQU	EDATA		0xC9+GT_I2C_CMD_START
GT_EX_SPECIAL0xCA_CMD				EQU	EDATA		0xCA+GT_I2C_CMD_START
GT_EX_SPECIAL0xCB_CMD				EQU	EDATA		0xCB+GT_I2C_CMD_START
GT_EX_SPECIAL0xCC_CMD				EQU	EDATA		0xCC+GT_I2C_CMD_START
GT_EX_SPECIAL0xCD_CMD				EQU	EDATA		0xCD+GT_I2C_CMD_START
GT_EX_SPECIAL0xCE_CMD				EQU	EDATA		0xCE+GT_I2C_CMD_START
GT_EX_SPECIAL0xCF_CMD				EQU	EDATA		0xCF+GT_I2C_CMD_START

GT_EX_SPECIAL0xD0_CMD				EQU	EDATA		0xD0+GT_I2C_CMD_START//Host Wr Cmd
GT_EX_SPECIAL0xD1_CMD				EQU	EDATA		0xD1+GT_I2C_CMD_START
GT_EX_SPECIAL0xD2_CMD				EQU	EDATA		0xD2+GT_I2C_CMD_START
GT_EX_SPECIAL0xD3_CMD				EQU	EDATA		0xD3+GT_I2C_CMD_START
GT_EX_SPECIAL0xD4_CMD				EQU	EDATA		0xD4+GT_I2C_CMD_START
GT_EX_SPECIAL0xD5_CMD				EQU	EDATA		0xD5+GT_I2C_CMD_START
GT_EX_SPECIAL0xD6_CMD				EQU	EDATA		0xD6+GT_I2C_CMD_START
GT_EX_SPECIAL0xD7_CMD				EQU	EDATA		0xD7+GT_I2C_CMD_START
GT_EX_SPECIAL0xD8_CMD				EQU	EDATA		0xD8+GT_I2C_CMD_START
GT_EX_SPECIAL0xD9_CMD				EQU	EDATA		0xD9+GT_I2C_CMD_START
GT_EX_SPECIAL0xDA_CMD				EQU	EDATA		0xDA+GT_I2C_CMD_START
GT_EX_SPECIAL0xDB_CMD				EQU	EDATA		0xDB+GT_I2C_CMD_START
GT_EX_SPECIAL0xDC_CMD				EQU	EDATA		0xDC+GT_I2C_CMD_START
GT_EX_SPECIAL0xDD_CMD				EQU	EDATA		0xDD+GT_I2C_CMD_START
GT_EX_SPECIAL0xDE_CMD				EQU	EDATA		0xDE+GT_I2C_CMD_START
GT_EX_SPECIAL0xDF_CMD				EQU	EDATA		0xDF+GT_I2C_CMD_START

GT_EX_0x41E4_CMD					EQU	EDATA		0xE4+GT_I2C_CMD_START

#define								GT_PAGE_CMD		0xFF
GT_EX_PAGE_CMD						EQU	EDATA		GT_PAGE_CMD+GT_I2C_CMD_START
	#define		GT_DEVIDE_MODE_DEVELOP				0xEF
	#define		GT_DEVIDE_MODE_DEVELOP_WORD			0xFFEF

#endif	//__CTI2C_GOODIX_DEF_H__