;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTUART_MAIN_H__
#define	__CTUART_MAIN_H__

EXTERN	CODE	(CTUART_Init)
EXTERN	CODE	(CTUART_PRINT_PROM_U8)
EXTERN	CODE	(CTUART_PRINT_EXSRAM_U8)
EXTERN	CODE	(_CTUART_PRINT_U8)
EXTERN	CODE	(_CTUART_PRINT)
EXTERN	CODE	(CTUART_PRINTLN)
EXTERN	CODE	(CTUART_PRINTSPACE)
EXTERN	CODE	(CTUART_Send)

#endif	//__CTUART_MAIN_H__