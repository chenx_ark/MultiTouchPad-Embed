;******************************************************************************
;* Project: MCU OS for Cap touch using Sitronix A8008 IC.
;* Author: Benson Shen (Jun. 20, 2013)
;* Copyright (C) 2013, Sitronix Tech. Corp.
;******************************************************************************

#ifndef	__CTI2C_FUNCTABLE_H__
#define	__CTI2C_FUNCTABLE_H__

//;******************************************************************************
//;		Function Table For OS To CmdBuf
//;******************************************************************************
//#if(CTMOS_I2C_REG_MAP_DIS==0)
//EXTERN	CODE	(CTI2C_OsSetFirmwareVersion)//St
//EXTERN	CODE	(CTI2C_OsSetFWReversion)//St
//EXTERN	CODE	(CTI2C_OsSetInitFwReversion)//St
//#endif//(CTMOS_I2C_REG_MAP_DIS==0)

#if(DEVICE_I2C_PROTOCOL==DEVICE_I2C_SITRONIX_A)
#if(CTMOS_I2C_PTC_SHRINK==0)
EXTERN	CODE	(CTI2C_OsSetSensingCounterInc)//St
#endif//(CTMOS_I2C_PTC_SHRINK==0)
#endif//(DEVICE_I2C_PROTOCOL==DEVICE_I2C_SITRONIX_A)
////EXTERN	CODE	(CTI2C_OsSetStatus_Device)
////EXTERN	CODE	(CTI2C_OsSetStatus_Error)
////EXTERN	CODE	(CTI2C_OsSetDeviceCtrl)
//
//
//;******************************************************************************
//;		Function Table For CmdBuf To OS
//;******************************************************************************
#if(DEVICE_I2C_PROTOCOL==DEVICE_I2C_SITRONIX_A)
EXTERN	CODE	(CTI2C_OsGetResolution)
#endif//(DEVICE_I2C_PROTOCOL==DEVICE_I2C_SITRONIX_A)
////EXTERN	CODE	(CTI2C_OsGetDeviceStatus)
//
//
//
//;******************************************************************************
//;		Function Table
//;******************************************************************************
////EXTERN	CODE	(CTI2C_WrCmdRangeCk)
EXTERN	CODE	(CTI2C_HostWrCmdParser)

EXTERN	CODE	(CTI2C_OsSetStatus)

//EXTERN	CODE	(CTI2C_OsSetTimeoutToIdle)
EXTERN	CODE	(CTI2C_OsGetTimeToIdle)

#if(DEVICE_I2C_PROTOCOL==DEVICE_I2C_SITRONIX_A)
#if(CTMOS_I2C_PTC_SHRINK==0)
EXTERN	CODE	(CTI2C_OsSetRawDataCrc)
#endif//(CTMOS_I2C_PTC_SHRINK==0)
#endif//(DEVICE_I2C_PROTOCOL==DEVICE_I2C_SITRONIX_A)
//
////EXTERN	CODE	(CTI2C_OsGetDeviceCtrl_Reset)
////EXTERN	CODE	(CTI2C_OsGetDeviceCtrl_PwrDn)
////EXTERN	CODE	(CTI2C_OsGetAlgoDataCtrl)
//EXTERN	CODE	(CTI2C_OsGetMultiTouchDisable)
//
////EXTERN	CODE	(CTI2C_OsGetMiscCtrl_DataInt)
////EXTERN	CODE	(CTI2C_OsGetMiscCtrl_CoordInt)
//
////EXTERN	CODE	(CTI2C_OsSetPageNumber)
#if(CTMOS_SMART_WAKEUP_EN==0)
#else//(CTMOS_SMART_WAKEUP_EN==0)
EXTERN	CODE	(CTI2C_OsSetSmartWakeupId)
#endif//(CTMOS_SMART_WAKEUP_EN==0)
EXTERN	CODE	(CTI2C_OsGetPageNumber)

//;******************************************************************************
//;		Function Table Init
//;******************************************************************************
EXTERN	CODE	(CTI2C_I2cRegBasicInit)
EXTERN	CODE	(CTI2C_I2cRegAdvInit)

;******************************************************************************
;		Function Table INT Control
;******************************************************************************
EXTERN	CODE	(CTI2C_IntSignalRelease)
EXTERN	CODE	(CTI2C_IntSignalTrigger)

#if(DEVICE_I2C_PROTOCOL==DEVICE_I2C_REPLACE)
#else//(DEVICE_I2C_PROTOCOL==DEVICE_I2C_REPLACE)
EXTERN	CODE	(CTI2C_HostRdCmdParser)
EXTERN	CODE	(CTI2C_OsGetDeviceCtrl_Reset)
EXTERN	CODE	(CTI2C_OsGetDeviceCtrl_PwrDn)
EXTERN	CODE	(CTI2C_PageCmdHandler)
#endif//(DEVICE_I2C_PROTOCOL==DEVICE_I2C_REPLACE)

#endif	//__CTI2C_FUNCTABLE_H__




