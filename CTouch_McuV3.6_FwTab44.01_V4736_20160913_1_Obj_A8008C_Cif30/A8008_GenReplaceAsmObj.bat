del CTouch_Mcu.uvgui_benson_shen.bak	
del CTouch_Mcu.uvgui.benson_shen
del CTouch_Mcu.uvopt
del CTouch_Mcu.uvproj
del CTouch_Mcu_uvopt.bak
del CTouch_Mcu_uvproj.bak

del CTouch_Mcu_I2cCif_Obj.uvgui_benson_shen.bak	
del CTouch_Mcu_I2cCif_Obj.uvgui.benson_shen
del CTouch_Mcu_I2cCif_Obj.uvopt
del CTouch_Mcu_I2cCif_Obj.uvproj
del CTouch_Mcu_I2cCif_Obj_uvopt.bak
del CTouch_Mcu_I2cCif_Obj_uvproj.bak

del CTouch_Mcu_I2cAsm_Obj.uvgui.benson_shen
del CTouch_Mcu_I2cAsm_Obj.uvgui_benson_shen.bak
del CTouch_Mcu_I2cAsm_Obj_uvopt.bak
del CTouch_Mcu_I2cAsm_Obj_uvproj.bak

del .\Lst\*.* /Q 
del .\Obj\*._ia 
del .\Obj\*.hex 
del .\Obj\*.bin 
del .\Obj\*.__I 
del .\Obj\*.SRC
del .\Obj\*.lnp
del .\Obj\*.plg
del .\Obj\CTouch_Mcu

rmdir .\Code\CSMTouch /S /Q
rmdir .\Code\CTALG /S /Q

copy .\Code\McuOs\CTI2C_Main.a51 .\Code\CTI2C_Main.a51
copy .\Code\McuOs\CTI2C_ExtendFuncTable.a51 .\Code\CTI2C_ExtendFuncTable.a51
copy .\Code\McuOs\CTI2C_FuncExtend.a51 .\Code\CTI2C_FuncExtend.a51
del .\Code\McuOs\*.* /Q
copy .\Code\*.* .\Code\McuOs\*.*
del .\Code\*.* /Q 


pause